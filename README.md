# JET CMS

## Development mode
Run `npm install` to install dependencies.
Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Development mode with AOT
Run `npm install` to install dependencies.
Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Production mode
Run `npm install` to install dependencies.
Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.