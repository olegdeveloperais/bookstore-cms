import { Injectable } from '@angular/core';
import { CanLoad, Router, CanActivate } from '@angular/router';
import { StorageItem } from '../../models/enums/storage-item';
import { LocalStorageService } from '../../services/local-storage.service';

@Injectable({
    providedIn: 'root',
})
export class CMSGuard implements CanActivate {
    constructor(
        private storage: LocalStorageService,
        private router: Router,
    ) { }

    canActivate(): boolean {
        const token = this.storage.getItem(StorageItem.TOKEN);
        if (!token) {
            // COMMENT BELOW FOR DEVELOPMENT WITH API
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }
}