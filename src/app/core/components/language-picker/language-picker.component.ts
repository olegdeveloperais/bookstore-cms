import { Component, OnInit, Input, HostListener, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { fadeInOut } from '../../animations/fade.animation';

@Component({
    selector: 'language-picker',
    templateUrl: './language-picker.component.html',
    styleUrls: ['./language-picker.component.scss'],
    animations: [fadeInOut],
})
export class LanguagePickerComponent {
    @Input() languages: string[];
    @Input() activeLanguage: string;
    @Output() change: EventEmitter<string> = new EventEmitter();
    isSelectorVisible: boolean;
    @ViewChild('activeLangRef') activeLangRef: ElementRef;

    onSelectLanguage(): void {
        setTimeout(() => {
            this.isSelectorVisible = true;
        });
    }

    onSetLanguage(lang: string): void {
        this.change.emit(lang);
    }

    @HostListener('window:click', ['$event'])
    clickEvent(e): void {
        if (this.activeLangRef.nativeElement.contains(e.target)) return;
        this.isSelectorVisible = false;
    }
}
