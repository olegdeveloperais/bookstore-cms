import { Component, OnInit, Input, ViewChild, Output, EventEmitter, Inject, PLATFORM_ID } from '@angular/core';
import { NotificationService } from '../../../services/notification.service';
import { isPlatformServer } from '@angular/common';
import { DocumentFileType, UploaderFileType } from '../../../models/enums/uploader-item';
import { UploaderService } from '../../../services/uploader.service';

@Component({
    selector: 'bookstore-upload-button',
    templateUrl: './upload-button.component.html',
    styleUrls: ['./upload-button.component.scss'],
})
export class UploadButtonComponent implements OnInit {
    file: File;
    fileName: string = '';
    filePath: string = '';
    timer: any = null;
    isPreview: boolean = false;
    isUploading: boolean = false;
    allowedExtensions: string[] = [];

    @Input() isMakePreview: boolean = true;
    @Input() supportedFormats: DocumentFileType[] = [];
    @Input() uploaderType: UploaderFileType = UploaderFileType.IMG;
    @Input() icon: string;
    @Input() color: string;
    @Input() folder: string; // ??? this parameter is not reading as a string!
    @Input() tooltip: string;
    @Input() disabled: boolean;
    @Output() onFileChanged: EventEmitter<any> = new EventEmitter();
    @Output() isloadingEvent: EventEmitter<boolean> = new EventEmitter();

    @ViewChild('fileSelector') fileSelector;

    constructor(
        @Inject(PLATFORM_ID) platformId: string,
        private uploaderService: UploaderService,
        private notificationService: NotificationService,
    ) {
        if (typeof (<any>window).MouseEvent === 'function' || isPlatformServer(platformId)) return;

        function CustomEvent(event, params): any {
            params = params || { bubbles: false, cancelable: false, detail: undefined };
            const evt: any = document.createEvent('CustomEvent');
            evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
            return evt;
        }
        CustomEvent.prototype = (<any>window).Event.prototype;
        (<any>window).CustomEvent = CustomEvent;
    }

    ngOnInit(): void {
        this.generateAllowedExtensions();
    }


    select(): void {
        if (this.disabled) {
            this.clear();
            this.disabled = false;
            return;
        }
        let event: any;
        if (typeof (<any>window).MouseEvent === 'function') {
            event = new MouseEvent('click', {});
        } else {
            event = new CustomEvent('click');
        }
        this.fileSelector.nativeElement.dispatchEvent(event);
    }

    clear(emit: boolean = true): void {
        this.file = undefined;
        this.fileName = '';
        this.filePath = '';
        this.isUploading = false;
        this.isPreview = false;
        this.fileSelector.nativeElement.value = '';
        this.isloadingEvent.emit(false);
        if (emit) {
            this.onFileChanged.emit('');
        }
    }

    async upload(): Promise<string> {
        try {
            if (this.file == undefined) return Promise.resolve(undefined);
            this.isloadingEvent.emit(true);
            const path = await this.uploaderService.uploadPublic(this.file).toPromise();
            this.isloadingEvent.emit(false);
            return path;
        } catch (error) {
            this.isloadingEvent.emit(false);
            this.notificationService.snackbar('Uploading error');
            return Promise.resolve(undefined);
        }
    }

    showLoader(): void {
        this.isloadingEvent.emit(true);
        const delay = 500;
        this.timer = setTimeout(() => {
            this.isUploading = true;
        }, delay);
    }

    hideLoader(): void {
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.isUploading = false;
        this.clear(false);
    }

    async loadFile(event): Promise<any> {
        const file = event.target.files[0];

        if (!file) {
            this.file = undefined;
            return;
        }
        if (file.type && this.supportedFormats.length && !this.supportedFormats.includes(file.type)) {
            this.notificationService.snackbar('Unsupported file format');
            this.file = undefined;
            return;
        }
        if (!file.type) {
            if (!this.validateFormat(file.name)) {
                this.notificationService.snackbar('Unsupported file format');
                this.file = undefined;
                return;
            }
        }
        this.file = file;
        if (this.folder) {
            try {
                this.showLoader();
                const path = await this.upload();
                this.onFileChanged.emit(path);
                this.hideLoader();
            } catch (e) {
                this.hideLoader();
                this.notificationService.snackbar('File upload error!');
            }
            return;
        }
        if (!this.isMakePreview) {
            this.fileName = file.name;
            this.onFileChanged.emit(file.name);
        } else {
            this.makePreview(event.target.files[0]);
        }
    }

    private makePreview(file: File): void {
        const reader = new FileReader();
        reader.onload = (event: any) => {
            this.isPreview = true;
            this.filePath = event.target.result;
            this.onFileChanged.emit(this.filePath);
        };
        reader.readAsDataURL(file);
    }

    private validateFormat(name: string): boolean {
        if (!this.supportedFormats.length) return true;
        const extension = name.split('.').pop().toLowerCase();
        return this.allowedExtensions.some(s => s == extension);
    }

    private generateAllowedExtensions(): void {
        for (const format of this.supportedFormats) {
            switch (format) {
                case DocumentFileType.DOC: this.allowedExtensions.push('doc');
                    break;
                case DocumentFileType.DOCX: this.allowedExtensions.push('docx');
                    break;
                case DocumentFileType.XLSX: this.allowedExtensions.push('xlsx');
                    break;
                case DocumentFileType.XLS: this.allowedExtensions.push('xls');
                    break;
                case DocumentFileType.PDF: this.allowedExtensions.push('pdf');
                    break;
                case DocumentFileType.TXT: this.allowedExtensions.push('txt');
                    break;
                case DocumentFileType.PPTX: this.allowedExtensions.push('pptx');
                    break;
                case DocumentFileType.PPT: this.allowedExtensions.push('ppt');
                    break;
                case DocumentFileType.AVI: this.allowedExtensions.push('avi');
                    break;
                case DocumentFileType.FLV: this.allowedExtensions.push('flv');
                    break;
                case DocumentFileType.MP4: this.allowedExtensions.push('mp4');
                    break;
                case DocumentFileType.MPEG: this.allowedExtensions.push('mpeg');
                    break;
                case DocumentFileType.OGG: this.allowedExtensions.push('ogg');
                    break;
                case DocumentFileType.WEBM: this.allowedExtensions.push('webm');
                    break;
                case DocumentFileType.JPEG: this.allowedExtensions.push('jpeg');
                    break;
                case DocumentFileType.JPG: this.allowedExtensions.push('jpg');
                    break;
                case DocumentFileType.PNG: this.allowedExtensions.push('png');
                    break;
                case DocumentFileType.TIFF: this.allowedExtensions.push('tiff');
                    break;
                default: '';
            }
        }
    }
}

