import { LanguagePickerComponent } from './language-picker/language-picker.component';
import { UploadButtonComponent } from './upload-button/upload-button.component';

export const APP_CORE_COMPONENTS = [
    LanguagePickerComponent,
    UploadButtonComponent,
];