import { trigger, state, style, animate, transition } from '@angular/animations';

export const widthInOut = trigger('widthInOut', [
    state('invisible', style({ overflow: 'hidden', width: 0 })),
    state('visible', style({ overflow: 'hidden', width: '*' })),
    transition('invisible => visible', [
        style({ overflow: 'hidden', width: 0 }),
        animate('0.3s ease-in-out', style({ overflow: 'hidden', width: '*' })),
    ]),
    transition('visible => invisible', [
        style({ overflow: 'hidden', width: '*' }),
        animate('0.3s ease-in-out', style({ overflow: 'hidden', width: 0 })),
    ]),
]);