import { trigger, state, style, animate, transition } from '@angular/animations';

export const heightInOut = trigger('heightInOut', [
    state('false', style({ overflow: 'hidden', height: 0})),
    state('true', style({ overflow: 'hidden', height: '*'})),
    transition('false => true', [
        style({ overflow: 'hidden', height: 0 }),
        animate('0.3s ease-in-out', style({ overflow: 'hidden', height: '*' })),
    ]),
    transition('true => false', [
        style({ overflow: 'hidden', height: '*' }),
        animate('0.3s ease-in-out', style({ overflow: 'hidden', height: 0 })),
    ]),
]);

export const heightInOutNoOpacity = trigger('heightInOutNoOpacity', [
    state('false', style({ height: 0 })),
    state('true', style({ height: '*' })),
    transition('false => true', [
        style({ height: 0 }),
        animate('0.2s ease-in-out', style({ height: '*' })),
    ]),
    transition('true => false', [
        style({ height: '*' }),
        animate('0.2s ease-in-out', style({ height: 0 })),
    ]),
]);

export const heightIn = trigger('heightIn', [
    state('visible', style({ height: '*', opacity: 1 })),
    state('invisible', style({ height: 0, opacity: 0 })),
    transition('invisible => visible', [
        style({ height: 0, opacity: 0 }),
        animate('0.2s ease-out', style({ height: '*', opacity: 1 })),
    ]),
]);

