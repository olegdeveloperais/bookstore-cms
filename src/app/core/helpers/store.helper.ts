export class StoreHelper {
    static deleteStoreArrayItem(array, index): any[] {
        return [
            ...array.slice(0, index),
            ...array.slice(index + 1),
        ];
    }

    static updateStoreArrayObject(array, i, payload): any[] {
        return array.map((item, index) => {
            if (i !== index) {
                return item;
            }

            return {
                ...item,
                ...payload,
            };
        });
    }
}