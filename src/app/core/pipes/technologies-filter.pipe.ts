import { Pipe, PipeTransform } from '@angular/core';
import { ITechnology } from '../../models/interfaces/technology';

@Pipe({
    name: 'technologiesFilterPipe',
})
export class TechnologiesFilterPipe implements PipeTransform {
    transform(technologies: ITechnology[], value: string): ITechnology[] {
        if (!technologies || !technologies.length) return [];
        if (!value) return technologies;
        value = value.toLowerCase();
        return technologies.filter(i => i.label.toLowerCase().includes(value)).reverse();
    }
}