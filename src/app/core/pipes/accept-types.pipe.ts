import { Pipe, PipeTransform } from '@angular/core';
import { DocumentFileType } from '../../models/enums/uploader-item';

@Pipe({
    name: 'acceptTypes',
})

export class AcceptTypesPipe implements PipeTransform {
    DocumentFileType = DocumentFileType;
    transform(value: DocumentFileType[], isAccept: boolean = false): string {
        if (!value) return;
        const keys: string[] = [];
        for (const key of value) {
            for (const i in this.DocumentFileType) {
                if (key == this.DocumentFileType[i])
                    keys.push(isAccept ? `.${i.toLowerCase()}` : i.toLowerCase());
            }
        }
        return keys.join(', ');
    }
}