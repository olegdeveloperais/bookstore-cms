import { Pipe, PipeTransform } from '@angular/core';
import { ISEOSettings, ISEOSettingsValue, IOGSettings } from '../../models/interfaces/seo-settings';

@Pipe({
    name: 'seoFilterPipe',
})
export class SeoFilterPipe implements PipeTransform {
    transform(settings: ISEOSettings[], value: string): ISEOSettings[] {
        if (!settings || !settings.length) return [];
        if (!value) return settings;
        value = value.toLowerCase();
        // tslint:disable-next-line:max-line-length
        return settings.filter(i => i.label.toLowerCase().includes(value) || i.url.toLowerCase().includes(value) || i.statusCode.toString().includes(value)).reverse();
    }
}

@Pipe({
    name: 'seoActiveValue',
})
export class SEOActiveValue implements PipeTransform {
    transform(values: ISEOSettingsValue[], activeLanguage: string, field: string): string {
        if (!values || !values.length || !activeLanguage || !field) return '';
        const v = values.find(v => v.lang === activeLanguage);
        if (!v) return '';
        return v[field];
    }
}

@Pipe({
    name: 'ogPipe',
})
export class OpenGraphPipe implements PipeTransform {
    transform(settings: IOGSettings): boolean {
        if (!settings) return false;
        return !!(settings.title || settings.keywords || settings.description || settings.image);
    }
}
