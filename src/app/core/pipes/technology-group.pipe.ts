import { Pipe, PipeTransform } from '@angular/core';
import { TechnologyGroup } from '../../models/enums/technology-group';

@Pipe({ name: 'technologyGroup' })
export class TechnologyGroupPipe implements PipeTransform {
    transform(group: TechnologyGroup): string {
        switch (group) {
            case TechnologyGroup.Backend: return 'Backend';
            case TechnologyGroup.Frontend: return 'Frontend';
        }
    }
}