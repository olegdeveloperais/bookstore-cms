import { Pipe, PipeTransform } from '@angular/core';
import { Plugins } from '../../models/enums/plugins';

@Pipe({
    name: 'pluginNamePipe',
})
export class PluginNamePipe implements PipeTransform {
    transform(plugin: Plugins): string {
        switch (plugin) {
            case Plugins.Translator: return 'Translator';
            case Plugins.SEOManagement: return 'SEO Management';
            case Plugins.Media: return 'Media';
        }
    }
}