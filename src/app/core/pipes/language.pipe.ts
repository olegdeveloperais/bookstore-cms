import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'languagesFilterPipe',
})
export class LanguagesFilterPipe implements PipeTransform {
    transform(languages: string[], activeLanguage: string): string[] {
        if (!languages || !languages.length || !activeLanguage) return [];
        return languages.filter(l => l !== activeLanguage);
    }
}