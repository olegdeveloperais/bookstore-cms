import { Pipe, PipeTransform } from '@angular/core';
import { ProjectCategory } from '../../models/enums/project-category';

@Pipe({ name: 'projectCategory' })
export class ProjectCategoryPipe implements PipeTransform {
    transform(category: ProjectCategory): string {
        if (category == null) return;
        switch (category) {
            case ProjectCategory.Enterprise: return 'Enterprise solution';
            case ProjectCategory.Data: return 'Data';
            case ProjectCategory.Ecommerce: return 'E-commerce';
            case ProjectCategory.Healthcare: return 'Healthcare';
            case ProjectCategory.Information: return 'Information';
            case ProjectCategory.Blockchain: return 'Blockchain';
            case ProjectCategory.Booking: return 'Booking&event platforms';
            default: return '';
        }
    }
}