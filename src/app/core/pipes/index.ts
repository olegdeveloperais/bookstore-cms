import { LanguagesFilterPipe } from './language.pipe';
import { TranslatorActiveValue, TranslatorFilterPipe } from './translator.pipe';
import { FormErrorPipe } from './form-error.pipe';
import { AcceptTypesPipe } from './accept-types.pipe';
import { ApiUrlPipe } from './apiUrl.pipe';
import { ProjectCategoryPipe } from './project-category.pipe';
import { SeoFilterPipe, SEOActiveValue, OpenGraphPipe } from './seo.pipe';
import { FilterTeamPipe } from './filter-team.pipe';
import { FilterBooksPipe } from './filter-books.pipe';
import { PluginNamePipe } from './plugin-name.pipe';
import { EnumKeyPipe } from './enum.pipe';
import { TechnologyGroupPipe } from './technology-group.pipe';
import { TechnologiesFilterPipe } from './technologies-filter.pipe';

export const APP_CORE_PIPES = [
    LanguagesFilterPipe,
    TranslatorActiveValue,
    TranslatorFilterPipe,
    FormErrorPipe,
    AcceptTypesPipe,
    ApiUrlPipe,
    ProjectCategoryPipe,
    SeoFilterPipe,
    FilterTeamPipe,
    FilterBooksPipe,
    PluginNamePipe,
    EnumKeyPipe,
    TechnologyGroupPipe,
    TechnologiesFilterPipe,
    SEOActiveValue,
    OpenGraphPipe,
];
