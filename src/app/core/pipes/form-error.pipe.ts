import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'formError',
})
export class FormErrorPipe implements PipeTransform {
    transform(errorObj: any): string {
        if (!errorObj) return;
        if (errorObj.required) return 'Field is required';
        if (errorObj.pattern) return 'Incorrect input format';
        if (errorObj.keyTaken) return 'error.form.key-exist';
        if (errorObj.minlength) return `error.form.min-length`;
        if (errorObj.maxlength) return `error.form.max-length`;
        if (errorObj.email) return 'Incorrect email';
    }
}