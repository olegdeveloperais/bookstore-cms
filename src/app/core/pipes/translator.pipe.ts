import { Pipe, PipeTransform } from '@angular/core';
import { IDictionaryItemValue, IDictionaryItem } from '../../models/interfaces/dictionary-item';

@Pipe({
    name: 'translatorActiveValue',
})
export class TranslatorActiveValue implements PipeTransform {
    transform(values: IDictionaryItemValue[], activeLanguage: string): string {
        if (!values || !values.length || !activeLanguage) return '';
        const v = values.find(v => v.lang === activeLanguage);
        if (!v) return '';
        return v.value;
    }
}

@Pipe({
    name: 'translatorFilterPipe',
})
export class TranslatorFilterPipe implements PipeTransform {
    transform(items: IDictionaryItem[], value: string): IDictionaryItem[] {
        if (!items || !items.length) return [];
        return items.filter(i => i.key.includes(value));
    }
}