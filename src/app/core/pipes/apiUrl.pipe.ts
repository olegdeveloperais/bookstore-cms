import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';

@Pipe({
    name: 'apiUrl',
})
export class ApiUrlPipe implements PipeTransform {
    constructor(
        protected sanitizer: DomSanitizer,
    ) { }

    transform(filePath: string): SafeResourceUrl {
        if (!filePath) return;
        const regExp = new RegExp('^(http|https)://', 'i');
        return filePath.startsWith('data:') || regExp.test(filePath) ?
            this.sanitizer.bypassSecurityTrustResourceUrl(filePath) :
            this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.apiUrl}/public/${filePath}`);
    }
}