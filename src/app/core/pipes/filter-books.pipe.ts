import { Pipe, PipeTransform } from '@angular/core';
// import { IProject } from '../../models/interfaces/project';
import { IBook } from '../../models/interfaces/book';

@Pipe({ name: 'filterBooks' })
export class FilterBooksPipe implements PipeTransform {
    transform(books: IBook[], searchStr: string): IBook[] {
        if (!books) return;
        if (!searchStr) return books;
        return books.filter(p => p.author.toLowerCase().includes(searchStr.toLowerCase()) ||
            p.title.toLowerCase().includes(searchStr.toLowerCase()));
    }
}