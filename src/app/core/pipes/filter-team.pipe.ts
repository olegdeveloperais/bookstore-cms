import { Pipe, PipeTransform } from '@angular/core';
import { IEmployeeInfo } from '../../models/interfaces/employee-info';

@Pipe({ name: 'filterTeam' })
export class FilterTeamPipe implements PipeTransform {
    transform(team: IEmployeeInfo[], searchStr: string, activeLang: string): IEmployeeInfo[] {
        if (!team) return;
        if (!searchStr) return team;
        return team.filter(t => {
            const val = t.values.find(v => v.lang === activeLang);
            if (!val) return false;
            return `${val.firstName} ${val.lastName} ${val.position}`.toLowerCase().includes(searchStr.toLowerCase());
        });
    }
}