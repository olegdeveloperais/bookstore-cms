import { AbstractControl, ValidatorFn } from '@angular/forms';

export function WebSiteValidator(control: AbstractControl): { [key: string]: boolean } {
    // tslint:disable-next-line
    const pattern = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
    const site = control.value;
    if (!site) return null;
    return pattern.test(String(site).toLowerCase()) ? null : { site: true };
}

export function ExistedItemValidator(items: any[]): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        const item = control.value;
        if (!items || !item) return null;

        return items.includes(String(item).toLowerCase()) ? { exist: true } : null;
    };
}