import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { APP_CORE_PIPES } from './pipes';
import { APP_CORE_DIRECTIVES } from './directives';
import { APP_CORE_COMPONENTS } from './components';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { APP_CORE_MODALS } from './modals/index';
import {
    MatFormFieldModule, MatInputModule, MatDialogModule,
    MatButtonModule, MatIconModule, MatTooltipModule,
} from '@angular/material';
import { LanguagesEditorComponent } from './modals/languages-editor/languages-editor.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatDialogModule,
        MatButtonModule,
        MatIconModule,
        MatTooltipModule,
    ],
    declarations: [
        ...APP_CORE_PIPES,
        ...APP_CORE_DIRECTIVES,
        ...APP_CORE_COMPONENTS,
        ...APP_CORE_MODALS,
        LanguagesEditorComponent,
    ],
    entryComponents: [
        ...APP_CORE_MODALS,
    ],
    exports: [
        ...APP_CORE_PIPES,
        ...APP_CORE_DIRECTIVES,
        ...APP_CORE_COMPONENTS,
        ...APP_CORE_MODALS,
    ],
    providers: [],
})
export class AppCoreModule { }