import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { NotificationService } from '../../../services/notification.service';

@Component({
    selector: 'ais-add-book',
    templateUrl: './add-book.component.html',
    styleUrls: ['./add-book.component.scss'],
})
export class AddBookComponent implements OnInit {
    @ViewChild('submitButton')
    title: string;
    content: string;
    value: any;
    form: FormGroup;
    previewUrl: string;

    constructor(
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<AddBookComponent>,
        private notificationService: NotificationService,
    ) { }

    ngOnInit(): void {
        this.initForm();
    }

    initForm(): void {
        this.form = this.fb.group({
            author: 'Author_',
            title: 'Title_',
            description: 'Description_',
            price: 9.99,
            img: ['', Validators.required],
        });
    }

    onReset(): void {
        this.initForm();
    }

    onFileChanged(e): void {
        this.form.controls.img.patchValue(e);
        this.previewUrl = `http://localhost:3000/public/${e}`;
    }

    onSubmit(e): void {
        if (!this.form.get('img').value) {
            this.notificationService.snackbar('Please add preview image');
            return;
        }
        if (this.form.invalid) {
            this.notificationService.snackbar('Please, fill in required fields');
            return;
        }
        if (this.form.valid) {
            this.dialogRef.close(this.form.value);
        }
    }
}
