import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { DocumentFileType } from '../../../models/enums/uploader-item';

@Component({
    selector: 'dictionary-editor',
    templateUrl: 'dictionary-editor.component.html',
    styleUrls: ['dictionary-editor.component.scss'],
})
export class DictionaryEditorComponent implements OnInit {
    key: string;
    value: string;
    form: FormGroup;
    isNew?: boolean;
    isMedia: boolean = false;
    isLoading: boolean;
    supportedFormats: DocumentFileType[] = [
        DocumentFileType.DOC, DocumentFileType.DOCX,
        DocumentFileType.JPEG, DocumentFileType.JPG, DocumentFileType.PDF,
        DocumentFileType.PNG, DocumentFileType.TXT, DocumentFileType.XLS,
        DocumentFileType.XLSX,
    ];

    constructor(
        private formBuilder: FormBuilder,
        private ref: MatDialogRef<DictionaryEditorComponent>,
    ) { }

    ngOnInit(): void {
        this.form = this.formBuilder.group({
            key: [this.key || '', [Validators.required]],
            value: [this.value || '', [Validators.required]],
        });
    }

    onSubmit(): void {
        if (this.form.invalid) return;
        this.ref.close(this.form.value);
    }


}