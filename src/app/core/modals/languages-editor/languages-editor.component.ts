import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { ExistedItemValidator } from '../../../core/validators/custom.validators';

@Component({
    selector: 'languages-editor',
    templateUrl: './languages-editor.component.html',
    styleUrls: ['./languages-editor.component.scss'],
})
export class LanguagesEditorComponent implements OnInit {
    control: FormControl = new FormControl('', Validators.required);
    languages: string[] = [];
    form: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private dialogRef: MatDialogRef<LanguagesEditorComponent>,
    ) { }

    ngOnInit(): void {
        this.form = this.formBuilder.group({
            lang: ['', [
                Validators.required,
                ExistedItemValidator(this.languages),
            ]],
        });
    }

    setLanguages(languages: string[]): void {
        if (languages && languages.length) {
            this.languages = languages.map(l => l.toLowerCase());
        }
    }

    onSubmit(): void {
        if (this.form.invalid) return;
        this.dialogRef.close(this.form.value.lang);
    }
}
