import { DictionaryEditorComponent } from './dictionary-editor/dictionary-editor.component';
import { LanguagesEditorComponent } from './languages-editor/languages-editor.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { TeamSelectorComponent } from './team-selector/team-selector.component';
import { EmailDialogComponent } from './email-dialog/email-dialog.component';
import { AddBookComponent } from './add-book/add-book.component';

export const APP_CORE_MODALS = [
    DictionaryEditorComponent,
    LanguagesEditorComponent,
    ConfirmDialogComponent,
    TeamSelectorComponent,
    EmailDialogComponent,
    AddBookComponent,
];