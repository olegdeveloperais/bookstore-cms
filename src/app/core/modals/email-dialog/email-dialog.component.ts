import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { ExistedItemValidator } from '../../../core/validators/custom.validators';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
    selector: 'email-dialog',
    templateUrl: './email-dialog.component.html',
    styleUrls: ['./email-dialog.component.scss'],
})
export class EmailDialogComponent implements OnInit {
    email: string;
    emails: string[];
    form: FormGroup;
    private _data$: BehaviorSubject<string> = new BehaviorSubject(null);

    constructor(
        private formBuilder: FormBuilder,
        private ref: MatDialogRef<EmailDialogComponent>,
    ) { }

    ngOnInit(): void {
        this.form = this.formBuilder.group({
            email: [this.email || '', [
                Validators.required,
                Validators.email,
                ExistedItemValidator(this.emails),
            ]],
        });
    }

    setEmails(emails: string[]): void {
        if (emails && emails.length) {
            this.emails = emails.map(e => e.toLowerCase());
        }
    }

    get data(): Observable<string> {
        return this._data$.asObservable();
    }

    onSubmit(): void {
        if (this.form.invalid) return;
        this._data$.next(this.form.value.email);
    }
}