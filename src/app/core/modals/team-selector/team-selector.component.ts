import { Component, Inject } from '@angular/core';
import { IEmployeeInfo, IClientEmployeeInfo } from '../../../models/interfaces/employee-info';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
    selector: 'team-selector',
    templateUrl: './team-selector.component.html',
    styleUrls: ['./team-selector.component.scss'],
})
export class TeamSelectorComponent {

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: IClientEmployeeInfo[],
        private dialogRef: MatDialogRef<TeamSelectorComponent>,
    ) { }

    onSelect(employee: IClientEmployeeInfo): void {
        this.dialogRef.close(employee);
    }

}
