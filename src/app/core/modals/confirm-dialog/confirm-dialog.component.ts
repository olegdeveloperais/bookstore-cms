import { Component } from '@angular/core';

@Component({
    selector: 'ais-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
    styleUrls: ['./confirm-dialog.component.scss'],
})
export class ConfirmDialogComponent {
    title: string;
    content: string;
    value: any;
}
