import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { APP_ROUTES } from './app.routing';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpInterceptor } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { appReducers } from './store';
import { metaReducers } from './store';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CMSModule } from './pages/cms/cms.module';
import { AuthModule } from './pages/auth/auth.module';
import { TranslatorEffects } from './store/translator/translator.effect';
import { AuthEffects } from './store/auth/auth.effect';
import { MediaEffects } from './store/media/media.effect';
import { SEOEffects } from './store/seo/seo.effect';
import { BooksEffects } from './store/books/books.effect';
import { TeamEffects } from './store/team/team.effect';
import { MatNativeDateModule, MatSnackBarModule } from '@angular/material';
import { FeedbacksEffects } from './store/feedbacks/feedbacks.effect';
// import { AccessInterceptor } from './services/access-interceptor.service';
import { TechnologiesEffects } from './store/technologies/technologies.effect';
import { NavigationEffects } from './store/navigation/navigation.effect';
// import { HttpInterceptorService } from './services/http-interceptors/http-interceptor.service';
import { httpInterceptorProviders } from './services/http-interceptors';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatNativeDateModule,
        MatSnackBarModule,
        RouterModule.forRoot(APP_ROUTES, { onSameUrlNavigation: 'reload' }),
        HttpClientModule,
        StoreModule.forRoot(appReducers, { metaReducers }),
        !environment.production ? StoreDevtoolsModule.instrument({ maxAge: 30 }) : [],
        EffectsModule.forRoot([
            AuthEffects,
            TranslatorEffects,
            MediaEffects,
            SEOEffects,
            BooksEffects,
            TeamEffects,
            FeedbacksEffects,
            TechnologiesEffects,
            NavigationEffects,
        ]),
        CMSModule,
        AuthModule,
    ],
    providers: [
        httpInterceptorProviders,
    ],
    bootstrap: [AppComponent],
})
export class AppModule { }
