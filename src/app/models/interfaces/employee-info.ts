export interface IEmployeeInfo {
    _id?: string;
    photo: string;
    showOnSite: boolean;
    isHead: boolean;
    values: IEmployeeInfoValue[];
}

export interface IEmployeeInfoValue {
    lang: string;
    firstName: string;
    lastName: string;
    position: string;
    description: string;
}

export interface IClientEmployeeInfo {
    _id?: string;
    firstName: string;
    lastName: string;
    position: string;
    isHead: boolean;
    description: string;
    photo: string;
}