export interface ITestimonial {
    photo: string;
    name: string;
    position?: string;
    comment: string;
    showOnSite: boolean;
}