import { PageStatusCode } from '../enums/page-status-code';

export interface ISEOSettings {
    _id?: string;
    label: string;
    url: string;
    statusCode?: PageStatusCode;
    redirectUrl?: string;
    indexing: boolean;
    followingLinks: boolean;
    googleSearchConsole?: string;
    yandexWebmaster?: string;
    values: ISEOSettingsValue[];
}

export interface ISEOSettingsValue {
    lang: string;
    og?: IOGSettings;
}

export interface IOGSettings {
    title?: string;
    keywords?: string;
    description?: string;
    image?: string;
}

export interface ISEOClientSettings {
    url: string;
    statusCode: PageStatusCode;
    indexing: boolean;
    followingLinks: boolean;
    redirectUrl?: string;
    googleSearchConsole?: string;
    yandexWebmaster?: string;
    og?: IOGSettings;
}