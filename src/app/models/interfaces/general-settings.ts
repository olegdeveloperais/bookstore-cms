import { Plugins } from '../enums/plugins';

export interface IGeneralSettings {
    plugins: Plugins[];
}