import { ProjectCategory } from '../enums/project-category';
import { ICoords } from './coords';
import { ITestimonial } from './testimonials';

export interface IProject {
    _id?: string;
    previewImage: string;
    projectColor?: string;
    images: string[];
    categories: ProjectCategory[];
    createdAt: string;
    name: string;
    routerUrl: string;
    showOnSite: boolean;
    technologies?: string[];
    website?: string;
    coords?: ICoords;
    values: IProjectValue[];
    logo?: string;
}

export interface IProjectValue {
    lang: string;
    excerpt?: string;
    aboutProject?: string;
    scope?: IScope;
    result?: IProjectResult;
    testimonial?: ITestimonial;
}

export interface IClientProject {
    _id?: string;
    previewImage: string;
    projectColor?: string;
    images: string[];
    categories: ProjectCategory[];
    createdAt: string;
    technologies?: string[];
    website?: string;
    name: string;
    logo?: string;
    routerUrl: string;
    excerpt?: string;
    aboutProject?: string;
    showOnSite: boolean;
    scope?: IScope;
    result?: IProjectResult;
    coords?: ICoords;
    testimonial?: ITestimonial;
}

// --------------------------------------------

export interface IProjectResult {
    text: string;
    team: string[];
    timeStart: string;
    timeEnd: string;
}

export interface IScope {
    text: string;
    images: string[];
}