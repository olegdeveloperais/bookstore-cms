export interface IPaginator {
    index: number;
    size: number;
}
