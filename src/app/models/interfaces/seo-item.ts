export interface ISEOItem {
    _id?: string;
    keywords: ISEOItemValue[];
    descriptions: ISEOItemValue[];
    titles: ISEOItemValue[];
}

export interface ISEOItemValue {
    lang: string;
    value: string;
}