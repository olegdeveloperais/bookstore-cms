export interface IDictionaryItem {
    _id?: string;
    key: string;
    values: IDictionaryItemValue[];
}

export interface IDictionaryItemValue {
    lang: string;
    value: string;
}