import { TechnologyGroup } from '../enums/technology-group';

export interface ITechnology {
    _id?: string;
    icon: string;
    label: string;
    group: TechnologyGroup;
    showOnHomepage: boolean;
}