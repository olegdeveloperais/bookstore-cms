export interface IMediaItem {
    _id?: string;
    key: string;
    value: string;
    alt?: string;
}