export enum PageStatusCode {
    OK = 200,
    MOVED_PERMANENTLY = 301,
    NOT_FOUND = 404,
}