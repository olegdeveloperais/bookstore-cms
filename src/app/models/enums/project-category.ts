export enum ProjectCategory {
    Enterprise,
    Healthcare,
    Information,
    Ecommerce,
    Data,
    Blockchain,
    Booking,
}