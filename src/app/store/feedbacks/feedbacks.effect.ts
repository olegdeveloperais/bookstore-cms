import { switchMap, map, tap, catchError, withLatestFrom } from 'rxjs/operators';
import { empty, of, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { MatDialog, MatDialogRef } from '@angular/material';
import * as FileSaver from 'file-saver';
import { HttpService } from '../../services/http.service';
import { ErrorService } from '../../services/error.service';
import { AppState } from '..';
import {
    FeedbacksActions, FeedbacksTypes, LoadFeedbacksAction, LoadFeedbacksSuccessAction,
    LoadFeedbacksErrorAction,
    LoadFeedbackAttachAction,
    DeleteFeedbackAction,
    DeleteFeedbackSuccessAction,
    DeleteFeedbackErrorAction,
    LoadFeedbackAttachSuccessAction,
    LoadFeedbackAttachErrorAction,
    LoadEmailsAction,
    LoadEmailsSuccessAction,
    LoadEmailsErrorAction,
    AddEmailAction,
    AddEmailSuccessAction,
    AddEmailErrorAction,
    DeleteEmailAction,
    DeleteEmailSuccessAction,
    DeleteEmailErrorAction,
} from './feedbacks.action';
import { getFeedbacksState } from './feedbacks.reducer';
import { EmailDialogComponent } from '../../core/modals/email-dialog/email-dialog.component';
import { ConfirmDialogComponent } from '../../core/modals/confirm-dialog/confirm-dialog.component';
import { UploaderService } from '../../services/uploader.service';

@Injectable()
export class FeedbacksEffects {

    emailDialogRef: MatDialogRef<EmailDialogComponent>;

    @Effect()
    loadFeedbacks$: Observable<FeedbacksActions> = this.actions$.pipe(
        ofType<LoadFeedbacksAction>(FeedbacksTypes.LoadFeedbacks),
        switchMap(() => {
            return this.http.loadFeedbacks().pipe(
                map(feedbacks => new LoadFeedbacksSuccessAction(feedbacks)),
                catchError(error => of(new LoadFeedbacksErrorAction(error))),
            );
        }),
    );

    @Effect()
    loadAttach$: Observable<FeedbacksActions> = this.actions$.pipe(
        ofType<LoadFeedbackAttachAction>(FeedbacksTypes.LoadAttach),
        map(action => action.payload),
        switchMap((feedback) => {
            return this.uploader.downloadFile(feedback.file).pipe(
                map(res => res.body),
                map(blob => new LoadFeedbackAttachSuccessAction({
                    fileName: `Attach from ${feedback.name}`,
                    blob: blob,
                })),
                catchError(error => of(new LoadFeedbackAttachErrorAction(error))),
            );
        }),
    );

    @Effect({ dispatch: false })
    loadAttachSuccess$: Observable<FeedbacksActions> = this.actions$.pipe(
        ofType<LoadFeedbackAttachSuccessAction>(FeedbacksTypes.LoadAttachSuccess),
        map(action => action.payload),
        tap(({ fileName, blob }) => {
            FileSaver.saveAs(blob, fileName);
        }),
        switchMap(() => empty()),
    );


    @Effect()
    deleteFeedback$: Observable<FeedbacksActions> = this.actions$.pipe(
        ofType<DeleteFeedbackAction>(FeedbacksTypes.DeleteFeedback),
        map(action => action.payload),
        switchMap(id => {
            const dialogRef = this.dialog.open(ConfirmDialogComponent);
            dialogRef.componentInstance.title = 'Are you sure?';
            dialogRef.componentInstance.value = id;
            return dialogRef.afterClosed();
        }),
        switchMap(id => {
            if (!id) return empty();
            return this.http.deleteFeedback(id).pipe(
                map(() => new DeleteFeedbackSuccessAction(id)),
                catchError(error => of(new DeleteFeedbackErrorAction(error))),
            );
        }),
    );

    @Effect()
    loadEmails$: Observable<FeedbacksActions> = this.actions$.pipe(
        ofType<LoadEmailsAction>(FeedbacksTypes.LoadEmails),
        switchMap(() => {
            return this.http.loadEmails().pipe(
                map(emails => new LoadEmailsSuccessAction(emails)),
                catchError(error => of(new LoadEmailsErrorAction(error))),
            );
        }),
    );

    @Effect()
    addEmail$: Observable<FeedbacksActions> = this.actions$.pipe(
        ofType<AddEmailAction>(FeedbacksTypes.AddEmail),
        withLatestFrom(this.store.select(getFeedbacksState).pipe(map(s => s.emails))),
        switchMap(([action, emails]) => {
            this.emailDialogRef = this.dialog.open(EmailDialogComponent);
            this.emailDialogRef.componentInstance.setEmails(emails);
            return this.emailDialogRef.componentInstance.data;
        }),
        switchMap((email: string) => {
            if (!email) return empty();
            return this.http.addEmail(email).pipe(
                map(() => {
                    this.emailDialogRef.close();
                    return new AddEmailSuccessAction(email);
                }),
                catchError(error => of(new AddEmailErrorAction(error))),
            );
        }),
    );

    @Effect()
    deleteEmail$: Observable<FeedbacksActions> = this.actions$.pipe(
        ofType<DeleteEmailAction>(FeedbacksTypes.DeleteEmail),
        map(action => action.payload),
        switchMap(email => {
            const dialogRef = this.dialog.open(ConfirmDialogComponent);
            dialogRef.componentInstance.title = 'Are you sure?';
            dialogRef.componentInstance.value = email;
            return dialogRef.afterClosed();
        }),
        switchMap(email => {
            if (!email) return empty();
            return this.http.deleteEmail(email).pipe(
                map(() => new DeleteEmailSuccessAction(email)),
                catchError(error => of(new DeleteEmailErrorAction(error))),
            );
        }),
    );

    @Effect({ dispatch: false })
    onError$: Observable<FeedbacksActions> = this.actions$.pipe(
        ofType<LoadFeedbacksErrorAction | DeleteFeedbackErrorAction | AddEmailErrorAction>
            (FeedbacksTypes.LoadFeedbacks, FeedbacksTypes.DeleteFeedbackSuccess, FeedbacksTypes.AddEmailError),
        map(action => action.payload),
        tap(e => {
            this.errorService.resolve(e);
        }),
    );


    constructor(
        private store: Store<AppState>,
        private actions$: Actions,
        private http: HttpService,
        private errorService: ErrorService,
        private dialog: MatDialog,
        private uploader: UploaderService,
    ) { }

}
