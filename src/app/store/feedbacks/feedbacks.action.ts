import { Action } from '@ngrx/store';
import { IFeedback } from '../../models/interfaces/feedback';

export enum FeedbacksTypes {
    LoadFeedbacks = '[Feedback] load feedbacks [...]',
    LoadFeedbacksSuccess = '[Feedback] load feedbacks [SUCCESS]',
    LoadFeedbacksError = '[Feedback] load feedbacks [ERROR]',

    LoadAttach = '[Feedback] load attach [...]',
    LoadAttachSuccess = '[Feedback] load attach [SUCCESS]',
    LoadAttachError = '[Feedback] load attach [ERROR]',

    DeleteFeedback = '[Feedback] delete feedback [...]',
    DeleteFeedbackSuccess = '[Feedback] delete feedback [Success]',
    DeleteFeedbackError = '[Feedback] delete feedback [Error]',

    LoadEmails = '[Feedback] load emails [...]',
    LoadEmailsSuccess = '[Feedback] load emails [SUCCESS]',
    LoadEmailsError = '[Feedback] load emails [ERROR]',

    AddEmail = '[Feedback] add email [...]',
    AddEmailSuccess = '[Feedback] add email [SUCCESS]',
    AddEmailError = '[Feedback] add email [ERROR]',

    DeleteEmail = '[Feedback] delete email [...]',
    DeleteEmailSuccess = '[Feedback] delete email [SUCCESS]',
    DeleteEmailError = '[Feedback] delete email [ERROR]',
}

// LOAD FEEDBACKS
export class LoadFeedbacksAction implements Action {
    readonly type = FeedbacksTypes.LoadFeedbacks;
}

export class LoadFeedbacksSuccessAction implements Action {
    readonly type = FeedbacksTypes.LoadFeedbacksSuccess;
    constructor(public payload: IFeedback[]) { }
}


export class LoadFeedbacksErrorAction implements Action {
    readonly type = FeedbacksTypes.LoadFeedbacksError;
    constructor(public payload: any) { }
}

// LOAD ATTACH
export class LoadFeedbackAttachAction implements Action {
    readonly type = FeedbacksTypes.LoadAttach;
    constructor(public payload: IFeedback) { }
}

export class LoadFeedbackAttachSuccessAction implements Action {
    readonly type = FeedbacksTypes.LoadAttachSuccess;
    constructor(public payload: { fileName: string, blob: Blob }) { }
}


export class LoadFeedbackAttachErrorAction implements Action {
    readonly type = FeedbacksTypes.LoadAttachError;
    constructor(public payload: any) { }
}

// DELETE FEEDBACK
export class DeleteFeedbackAction implements Action {
    readonly type = FeedbacksTypes.DeleteFeedback;
    constructor(public payload: string) { }
}

export class DeleteFeedbackSuccessAction implements Action {
    readonly type = FeedbacksTypes.DeleteFeedbackSuccess;
    constructor(public payload: string) { }
}

export class DeleteFeedbackErrorAction implements Action {
    readonly type = FeedbacksTypes.DeleteFeedbackError;
    constructor(public payload: any) { }
}

// LOAD EMAILS
export class LoadEmailsAction implements Action {
    readonly type = FeedbacksTypes.LoadEmails;
}

export class LoadEmailsSuccessAction implements Action {
    readonly type = FeedbacksTypes.LoadEmailsSuccess;
    constructor(public payload: string[]) { }
}


export class LoadEmailsErrorAction implements Action {
    readonly type = FeedbacksTypes.LoadEmailsError;
    constructor(public payload: any) { }
}

// ADD EMAILS
export class AddEmailAction implements Action {
    readonly type = FeedbacksTypes.AddEmail;
}

export class AddEmailSuccessAction implements Action {
    readonly type = FeedbacksTypes.AddEmailSuccess;
    constructor(public payload: string) { }
}


export class AddEmailErrorAction implements Action {
    readonly type = FeedbacksTypes.AddEmailError;
    constructor(public payload: any) { }
}

// DELETE EMAIL
export class DeleteEmailAction implements Action {
    readonly type = FeedbacksTypes.DeleteEmail;
    constructor(public payload: string) { }
}

export class DeleteEmailSuccessAction implements Action {
    readonly type = FeedbacksTypes.DeleteEmailSuccess;
    constructor(public payload: string) { }
}


export class DeleteEmailErrorAction implements Action {
    readonly type = FeedbacksTypes.DeleteEmailError;
    constructor(public payload: any) { }
}

export type FeedbacksActions
    = LoadFeedbacksAction
    | LoadFeedbacksSuccessAction
    | LoadFeedbacksErrorAction
    | LoadFeedbackAttachAction
    | LoadFeedbackAttachSuccessAction
    | LoadFeedbackAttachErrorAction
    | DeleteFeedbackAction
    | DeleteFeedbackSuccessAction
    | DeleteFeedbackErrorAction
    | LoadEmailsAction
    | LoadEmailsSuccessAction
    | LoadEmailsErrorAction
    | AddEmailAction
    | AddEmailSuccessAction
    | AddEmailErrorAction
    | DeleteEmailAction
    | DeleteEmailSuccessAction
    | DeleteEmailErrorAction
    ;