import { AppState } from '..';
import { StoreHelper } from '../../core/helpers/store.helper';
import { IFeedback } from '../../models/interfaces/feedback';
import { FeedbacksActions, FeedbacksTypes } from './feedbacks.action';

export class FeedbacksState {
    feedbacks: IFeedback[] = [];
    emails: string[] = [];
    isFeedbacksLoading: boolean;
}

export function reducer(state = new FeedbacksState(), action: FeedbacksActions): FeedbacksState {
    switch (action.type) {
        // ENABLE FEEDBACKS
        case FeedbacksTypes.LoadFeedbacks:
            return {
                ...state,
                isFeedbacksLoading: true,
            };
        case FeedbacksTypes.LoadFeedbacksSuccess:
            return {
                ...state,
                isFeedbacksLoading: false,
                feedbacks: action.payload,
            };
        case FeedbacksTypes.LoadFeedbacksError:
            return {
                ...state,
                isFeedbacksLoading: false,
                feedbacks: [],
            };
        // DELETE FEEDBACK
        case FeedbacksTypes.DeleteFeedbackSuccess:
            return {
                ...state,
                feedbacks: StoreHelper.deleteStoreArrayItem(
                    state.feedbacks,
                    state.feedbacks.findIndex(s => s._id === action.payload),
                ) || [],
            };
        // LOAD EMAILS
        case FeedbacksTypes.LoadEmails:
            return {
                ...state,
                isFeedbacksLoading: true,
            };
        case FeedbacksTypes.LoadEmailsSuccess:
            return {
                ...state,
                isFeedbacksLoading: false,
                emails: action.payload,
            };
        case FeedbacksTypes.LoadFeedbacksError:
            return {
                ...state,
                isFeedbacksLoading: false,
                emails: [],
            };
        // ADD EMAIL
        case FeedbacksTypes.AddEmailSuccess:
            return {
                ...state,
                emails: [...state.emails, action.payload],
            };
        // DELETE EMAIL
        case FeedbacksTypes.DeleteEmailSuccess:
            return {
                ...state,
                emails: StoreHelper.deleteStoreArrayItem(
                    state.emails,
                    state.emails.findIndex(email => email === action.payload),
                ) || [],
            };
        default: {
            return state;
        }
    }
}

export const getFeedbacksState = (state: AppState) => <FeedbacksState>state.feedbacks;