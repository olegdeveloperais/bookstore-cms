import { Action } from '@ngrx/store';
import { IDictionaryItem } from '../../models/interfaces/dictionary-item';

export enum TranslatorTypes {
    EnableSelector = '[ADMIN-TRANSLATOR] enable selector',

    ChangeLanguage = '[ADMIN-TRANSLATOR] change language',

    LoadLanguages = '[ADMIN-TRANSLATOR] load languages [...]',
    LoadLanguagesSuccess = '[ADMIN-TRANSLATOR] load languages [SUCCESS]',
    LoadLanguagesError = '[ADMIN-TRANSLATOR] load languages [ERROR]',

    LoadDictionary = '[ADMIN-TRANSLATOR] load dictionary [...]',
    LoadDictionarySuccess = '[ADMIN-TRANSLATOR] load dictionary [SUCCESS]',
    LoadDictionaryError = '[ADMIN-TRANSLATOR] load dictionary [ERROR]',

    AddDictionaryItem = '[ADMIN-TRANSLATOR] add dictionary item [...]',
    AddDictionaryItemSuccess = '[ADMIN-TRANSLATOR] add dictionary item [Success]',
    AddDictionaryItemError = '[ADMIN-TRANSLATOR] add dictionary item [Error]',

    EditDictionaryItem = '[ADMIN-TRANSLATOR] edit dictionary item [...]',
    EditDictionaryItemSuccess = '[ADMIN-TRANSLATOR] edit dictionary item [Success]',
    EditDictionaryItemError = '[ADMIN-TRANSLATOR] edit dictionary item [Error]',

    DeleteDictionaryItem = '[ADMIN-TRANSLATOR] delete dictionary item [...]',
    DeleteDictionaryItemSuccess = '[ADMIN-TRANSLATOR] delete dictionary item [Success]',
    DeleteDictionaryItemError = '[ADMIN-TRANSLATOR] delete dictionary item [Error]',

    AddLanguage = '[ADMIN-TRANSLATOR] add language[...]',
    AddLanguageSuccess = '[ADMIN-TRANSLATOR] add dlanguage [Success]',
    AddLanguageError = '[ADMIN-TRANSLATOR] add language [Error]',

    DeleteLanguage = '[ADMIN-TRANSLATOR] delete language [...]',
    DeleteLanguageSuccess = '[ADMIN-TRANSLATOR] language [Success]',
    DeleteLanguageError = '[ADMIN-TRANSLATOR] delete language [Error]',
}

// ENABLE SELECTOR
export class EnableTranslatorSelectorAction implements Action {
    readonly type = TranslatorTypes.EnableSelector;
    constructor(public payload: boolean) { }
}

// CHANGE LANGUAGE
export class ChangeLanguageAction implements Action {
    readonly type = TranslatorTypes.ChangeLanguage;
    constructor(public payload: string) { }
}

// LOAD LANGUAGES
export class LoadLanguagesAction implements Action {
    readonly type = TranslatorTypes.LoadLanguages;
}

export class LoadLanguagesSuccessAction implements Action {
    readonly type = TranslatorTypes.LoadLanguagesSuccess;
    constructor(public payload: string[]) { }
}

export class LoadLanguagesErrorAction implements Action {
    readonly type = TranslatorTypes.LoadLanguagesError;
    constructor(public payload: any) { }
}

// LOAD DICTIONARY
export class LoadDictionaryAction implements Action {
    readonly type = TranslatorTypes.LoadDictionary;
    constructor(public payload: string) { }
}

export class LoadDictionarySuccessAction implements Action {
    readonly type = TranslatorTypes.LoadDictionarySuccess;
    constructor(public payload: IDictionaryItem[]) { }
}

export class LoadDIctionaryErrorAction implements Action {
    readonly type = TranslatorTypes.LoadDictionaryError;
    constructor(public payload: any) { }
}

// ADD DICTIONARY ITEM
export class AddDictionaryItemAction implements Action {
    readonly type = TranslatorTypes.AddDictionaryItem;
}

export class AddDictionaryItemSuccessAction implements Action {
    readonly type = TranslatorTypes.AddDictionaryItemSuccess;
    constructor(public payload: IDictionaryItem) { }
}

export class AddDictionaryItemErrorAction implements Action {
    readonly type = TranslatorTypes.AddDictionaryItemError;
    constructor(public payload: any) { }
}

// EDIT DICTIONARY ITEM
export class EditDictionaryItemAction implements Action {
    readonly type = TranslatorTypes.EditDictionaryItem;
    constructor(public payload: IDictionaryItem) { }
}

export class EditDictionaryItemSuccessAction implements Action {
    readonly type = TranslatorTypes.EditDictionaryItemSuccess;
    constructor(public payload: IDictionaryItem) { }
}

export class EditDictionaryItemErrorAction implements Action {
    readonly type = TranslatorTypes.EditDictionaryItemError;
    constructor(public payload: any) { }
}

// DELETE DICTIONARY ITEM
export class DeleteDictionaryItemAction implements Action {
    readonly type = TranslatorTypes.DeleteDictionaryItem;
    constructor(public payload: string) { }
}

export class DeleteDictionaryItemSuccessAction implements Action {
    readonly type = TranslatorTypes.DeleteDictionaryItemSuccess;
    constructor(public payload: string) { }
}

export class DeleteDictionaryItemErrorAction implements Action {
    readonly type = TranslatorTypes.DeleteDictionaryItemError;
    constructor(public payload: any) { }
}

// ADD LANGUAGE
export class AddLanguageAction implements Action {
    readonly type = TranslatorTypes.AddLanguage;
}

export class AddLanguageSuccessAction implements Action {
    readonly type = TranslatorTypes.AddLanguageSuccess;
    constructor(public payload: string) { }
}

export class AddLanguageErrorAction implements Action {
    readonly type = TranslatorTypes.AddLanguageError;
    constructor(public payload: any) { }
}

// DELETE LANGUAGE
export class DeleteLanguageAction implements Action {
    readonly type = TranslatorTypes.DeleteLanguage;
    constructor(public payload: string) { }
}

export class DeleteLanguageSuccessAction implements Action {
    readonly type = TranslatorTypes.DeleteLanguageSuccess;
    constructor(public payload: string) { }
}

export class DeleteLanguageErrorAction implements Action {
    readonly type = TranslatorTypes.DeleteLanguageError;
    constructor(public payload: any) { }
}
export type TranslatorActions
    = EnableTranslatorSelectorAction
    | ChangeLanguageAction
    | LoadLanguagesAction
    | LoadLanguagesSuccessAction
    | LoadLanguagesErrorAction
    | LoadDictionaryAction
    | LoadDictionarySuccessAction
    | LoadDIctionaryErrorAction
    | AddDictionaryItemAction
    | AddDictionaryItemSuccessAction
    | AddDictionaryItemErrorAction
    | EditDictionaryItemAction
    | EditDictionaryItemSuccessAction
    | EditDictionaryItemErrorAction
    | DeleteDictionaryItemAction
    | DeleteDictionaryItemSuccessAction
    | DeleteDictionaryItemErrorAction
    | AddLanguageAction
    | AddLanguageSuccessAction
    | AddLanguageErrorAction
    | DeleteLanguageAction
    | DeleteLanguageSuccessAction
    | DeleteLanguageErrorAction
    ;