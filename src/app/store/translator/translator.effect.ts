import { switchMap, map, tap, catchError, withLatestFrom, skipWhile } from 'rxjs/operators';
import { empty, of, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { MatDialogRef, MatDialog } from '@angular/material';
import {
    LoadDictionarySuccessAction, LoadDIctionaryErrorAction, TranslatorActions, LoadDictionaryAction,
    TranslatorTypes,
    AddDictionaryItemAction,
    LoadLanguagesAction,
    LoadLanguagesSuccessAction,
    EditDictionaryItemSuccessAction,
    DeleteDictionaryItemErrorAction,
    AddLanguageAction,
    AddLanguageSuccessAction,
    AddLanguageErrorAction,
    DeleteLanguageAction,
    LoadLanguagesErrorAction,
} from './translator.action';
import { HttpService } from '../../services/http.service';
import { IDictionaryItem } from '../../models/interfaces/dictionary-item';
import { DictionaryEditorComponent } from '../../core/modals/dictionary-editor/dictionary-editor.component';
import {
    AddDictionaryItemSuccessAction, AddDictionaryItemErrorAction,
    ChangeLanguageAction,
} from './translator.action';
import { ErrorService } from '../../services/error.service';
import { Action, Store } from '@ngrx/store';
import {
    EditDictionaryItemAction, EditDictionaryItemErrorAction, DeleteDictionaryItemAction,
    DeleteDictionaryItemSuccessAction,
} from './translator.action';
import { AppState } from '..';
import { getTranslatorState } from './translator.reducer';
import { StoreHelper } from '../../core/helpers/store.helper';
import { DeleteLanguageSuccessAction, DeleteLanguageErrorAction } from './translator.action';
import { LanguagesEditorComponent } from '../../core/modals/languages-editor/languages-editor.component';
import { ConfirmDialogComponent } from '../../core/modals/confirm-dialog/confirm-dialog.component';

@Injectable()
export class TranslatorEffects {

    @Effect()
    loadDictionary$: Observable<TranslatorActions> = this.actions$.pipe(
        ofType<LoadDictionaryAction>(TranslatorTypes.LoadDictionary),
        map(action => action.payload),
        switchMap(lang => {
            return this.http.loadDictionary(lang).pipe(
                map(dictionary => new LoadDictionarySuccessAction(dictionary.reverse() || [])),
                catchError(error => of(new LoadDIctionaryErrorAction(error))),
            );
        }),
    );

    @Effect()
    addDictionaryItem$: Observable<TranslatorActions> = this.actions$.pipe(
        ofType<AddDictionaryItemAction>(TranslatorTypes.AddDictionaryItem),
        withLatestFrom(this.store.select(getTranslatorState).pipe(map(s => s.activeLanguage))),
        switchMap(([action, activeLanguage]) => {
            const dialogRef = this.dialog.open(DictionaryEditorComponent);
            dialogRef.componentInstance.isNew = true;
            return dialogRef.afterClosed().pipe(
                switchMap((v: { key: string, value: string }) => {
                    if (!v) return empty();
                    const item = {
                        key: v.key,
                        values: [
                            {
                                lang: activeLanguage,
                                value: v.value,
                            },
                        ],
                    };
                    return of(item);
                }),
            );
        }),
        switchMap((item: IDictionaryItem) => {
            return this.http.addDictionaryItem(item).pipe(
                map((res) => new AddDictionaryItemSuccessAction({ ...item, _id: res.id })),
                catchError(error => of(new AddDictionaryItemErrorAction(error))),
            );
        }),
    );

    @Effect()
    editDictionaryItem$: Observable<TranslatorActions> = this.actions$.pipe(
        ofType<EditDictionaryItemAction>(TranslatorTypes.EditDictionaryItem),
        map(action => action.payload),
        withLatestFrom(this.store.select(getTranslatorState).pipe(map(s => s.activeLanguage))),
        switchMap(([item, activeLang]) => {
            const dialogRef = this.dialog.open(DictionaryEditorComponent);
            dialogRef.componentInstance.key = item.key;
            const index = item.values.findIndex(v => v.lang === activeLang);
            dialogRef.componentInstance.value = index > -1 ? item.values[index].value : '';
            return dialogRef.afterClosed().pipe(
                switchMap((v: { key: string, value: string }) => {
                    if (!v) return empty();
                    if (index > -1) {
                        return of({
                            ...item,
                            key: v.key,
                            values: StoreHelper.updateStoreArrayObject(
                                item.values,
                                index, {
                                    lang: activeLang,
                                    value: v.value,
                                }),
                        });
                    } else {
                        return of({
                            ...item,
                            key: v.key,
                            values: [
                                ...item.values,
                                {
                                    lang: activeLang,
                                    value: v.value,
                                },
                            ],
                        });
                    }
                }),
            );
        }),
        switchMap(item => {
            if (!item) return empty();
            return this.http.updateDictionaryItem(item).pipe(
                map(() => new EditDictionaryItemSuccessAction(item)),
                catchError(error => of(new EditDictionaryItemErrorAction(error))),
            );
        }),
    );

    @Effect()
    deleteDictionaryItem$: Observable<TranslatorActions> = this.actions$.pipe(
        ofType<DeleteDictionaryItemAction>(TranslatorTypes.DeleteDictionaryItem),
        map(action => action.payload),
        switchMap(id => {
            const dialogRef = this.dialog.open(ConfirmDialogComponent);
            dialogRef.componentInstance.title = 'Are you sure?';
            dialogRef.componentInstance.value = id;
            return dialogRef.afterClosed();
        }),
        switchMap(id => {
            if (!id) return empty();
            return this.http.deleteDictionaryItem(id).pipe(
                map(() => new DeleteDictionaryItemSuccessAction(id)),
                catchError(error => of(new DeleteDictionaryItemErrorAction(error))),
            );
        }),
    );

    @Effect()
    addLanguage$: Observable<TranslatorActions> = this.actions$.pipe(
        ofType<AddLanguageAction>(TranslatorTypes.AddLanguage),
        withLatestFrom(this.store.select(getTranslatorState).pipe(map(s => s.languages))),
        switchMap(([action, languages]) => {
            const dialogRef = this.dialog.open(LanguagesEditorComponent);
            dialogRef.componentInstance.setLanguages(languages);
            return dialogRef.afterClosed();
        }),
        switchMap((lang: string) => {
            if (!lang) return empty();
            return this.http.addLanguage(lang).pipe(
                switchMap(() => of(
                    new AddLanguageSuccessAction(lang),
                    new ChangeLanguageAction(lang),
                )),
                catchError(error => of(new AddLanguageErrorAction(error))),
            );
        }),
    );

    @Effect()
    loadLanguages$: Observable<Action> = this.actions$.pipe(
        ofType<LoadLanguagesAction>(TranslatorTypes.LoadLanguages),
        switchMap(() => {
            return this.http.loadLanguages().pipe(
                switchMap((languages) => of(
                    new LoadLanguagesSuccessAction(languages),
                    new ChangeLanguageAction(languages && languages.length ? languages[0] : undefined),
                    new LoadDictionaryAction(languages && languages.length ? languages[0] : undefined),
                )),
                catchError(error => of(new LoadLanguagesErrorAction(error))),
            );
        }),
    );
    @Effect()
    deleteLanguage$: Observable<TranslatorActions> = this.actions$.pipe(
        ofType<DeleteLanguageAction>(TranslatorTypes.DeleteLanguage),
        map(action => action.payload),
        withLatestFrom(this.store.select(getTranslatorState).pipe(map(s => s.languages))),
        switchMap(([lang, languages]) => {
            const dialogRef = this.dialog.open(ConfirmDialogComponent);
            dialogRef.componentInstance.title = 'Are you sure?';
            dialogRef.componentInstance.value = [lang, languages];
            return dialogRef.afterClosed();
        }),
        switchMap(([lang, languages]) => {
            if (!lang || !languages) return empty();
            return this.http.deleteLanguage(lang).pipe(
                switchMap(() => {
                    return of(
                        new DeleteLanguageSuccessAction(lang),
                        new ChangeLanguageAction(languages[0]),
                    );
                }),
                catchError(error => of(new DeleteLanguageErrorAction(error))),
            );
        }),
    );

    @Effect({ dispatch: false })
    onError$: Observable<TranslatorActions> = this.actions$.pipe(
        ofType<LoadDIctionaryErrorAction | AddDictionaryItemErrorAction | LoadDIctionaryErrorAction
            | EditDictionaryItemErrorAction | DeleteDictionaryItemErrorAction | AddLanguageErrorAction
            | DeleteLanguageErrorAction | LoadLanguagesErrorAction>
            (TranslatorTypes.LoadDictionaryError, TranslatorTypes.AddDictionaryItemError,
            TranslatorTypes.LoadDictionaryError, TranslatorTypes.EditDictionaryItemError,
            TranslatorTypes.DeleteDictionaryItemError, TranslatorTypes.AddLanguageError,
            TranslatorTypes.DeleteLanguageError, TranslatorTypes.LoadLanguagesError),
        map(action => action.payload),
        tap(e => {
            this.errorService.resolve(e);
        }),
    );

    constructor(
        private actions$: Actions,
        private http: HttpService,
        private dialog: MatDialog,
        private store: Store<AppState>,
        private errorService: ErrorService,
    ) { }

}
