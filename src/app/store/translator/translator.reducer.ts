import { TranslatorActions, TranslatorTypes } from './translator.action';
import { IDictionaryItem } from '../../models/interfaces/dictionary-item';
import { AppState } from '../index';
import { StoreHelper } from '../../core/helpers/store.helper';
import { Observable } from 'rxjs/Observable';

export class TranslatorState {
    isSelectorEnabled: boolean = true;
    languages: string[];
    isLanguagesLoading: boolean;
    activeLanguage: string;
    dictionary: IDictionaryItem[];
    isDictionaryLoading: boolean;
}

export function reducer(state = new TranslatorState(), action: TranslatorActions): TranslatorState {
    switch (action.type) {
        // ENABLE SELECTOR
        case TranslatorTypes.EnableSelector:
            return {
                ...state,
                isSelectorEnabled: action.payload,
            };
        // SET ACTIVE LANGUAGE
        case TranslatorTypes.ChangeLanguage:
            return {
                ...state,
                activeLanguage: action.payload,
            };
        // LOAD LANGUAGES
        case TranslatorTypes.LoadLanguages:
            return {
                ...state,
                isLanguagesLoading: true,
            };
        case TranslatorTypes.LoadLanguagesSuccess:
            return {
                ...state,
                languages: action.payload,
                isLanguagesLoading: false,
            };
        case TranslatorTypes.LoadLanguagesError:
            return {
                ...state,
                languages: [],
                isLanguagesLoading: false,
            };
        // LOAD DICTIONARY
        case TranslatorTypes.LoadDictionary:
            return {
                ...state,
                isDictionaryLoading: true,
            };
        case TranslatorTypes.LoadDictionarySuccess:
            return {
                ...state,
                dictionary: action.payload,
                isDictionaryLoading: false,
            };
        case TranslatorTypes.LoadDictionaryError:
            return {
                ...state,
                dictionary: undefined,
                isDictionaryLoading: false,
            };
        // ADD DICTIONARY ITEM
        case TranslatorTypes.AddDictionaryItemSuccess:
            return {
                ...state,
                dictionary: [action.payload, ...state.dictionary],
            };
        // EDIT DICTIONARY ITEM
        case TranslatorTypes.EditDictionaryItemSuccess:
            return {
                ...state,
                dictionary: StoreHelper.updateStoreArrayObject(
                    state.dictionary,
                    state.dictionary.findIndex(s => s._id === action.payload._id),
                    action.payload,
                ),
            };
        // DELETE DICTIONARY ITEM
        case TranslatorTypes.DeleteDictionaryItemSuccess:
            return {
                ...state,
                dictionary: StoreHelper.deleteStoreArrayItem(
                    state.dictionary,
                    state.dictionary.findIndex(s => s._id === action.payload),
                ),
            };
        // ADD LANGUAGE
        case TranslatorTypes.AddLanguageSuccess:
            return {
                ...state,
                languages: [...state.languages, action.payload],
            };
        // DELETE LANGUAGE
        case TranslatorTypes.DeleteLanguageSuccess:
            return {
                ...state,
                languages: StoreHelper.deleteStoreArrayItem(
                    state.languages,
                    state.languages.findIndex(s => s === action.payload),
                ),
            };
        default: {
            return state;
        }
    }
}

export const getTranslatorState = (state: AppState) => <TranslatorState>state.translator;