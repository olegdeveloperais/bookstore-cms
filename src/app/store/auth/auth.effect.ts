import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { tap, map, switchMap, catchError } from 'rxjs/operators';
import { Observable, empty, of } from 'rxjs';
import {
    AuthActions, LoginAction, AuthTypes, LoginErrorAction, LoginSuccessAction,
    LogoutAction,
} from './auth.action';
import { AuthService } from '../../services/auth.service';
import { LocalStorageService } from '../../services/local-storage.service';
import { NotificationService } from '../../services/notification.service';
import { StorageItem } from '../../models/enums/storage-item';
import { Router } from '@angular/router';

@Injectable()
export class AuthEffects {
    @Effect()
    login$: Observable<AuthActions> = this.actions$.pipe(
        ofType<LoginAction>(AuthTypes.Login),
        map(action => action.payload),
        switchMap(creds => this.authService.login(creds).pipe(
            map(token => new LoginSuccessAction(token)),
            catchError(error => of(new LoginErrorAction(error))),
        )),
    );

    @Effect({ dispatch: false })
    onLoginSuccess$: Observable<Action> = this.actions$.pipe(
        ofType<LoginSuccessAction>(AuthTypes.LoginSuccess),
        map(action => action.payload),
        tap((token: string) => {
            if (!token) return;
            this.localStorage.setItem(StorageItem.TOKEN, token);
            this.router.navigate(['/welcome']);
        }),
        switchMap(() => empty()),
    );

    @Effect({ dispatch: false })
    onLogout$: Observable<Action> = this.actions$.pipe(
        ofType<LogoutAction>(AuthTypes.Logout),
        tap(token => {
            this.localStorage.removeItem(StorageItem.TOKEN);
            this.router.navigate(['/login']);
        }),
    );

    @Effect({ dispatch: false })
    onError$: Observable<Action> = this.actions$.pipe(
        ofType<LoginErrorAction>(AuthTypes.LoginError),
        map(action => action.payload),
        tap(e => {
            console.error(e);
            this.notificationService.snackbar('Incorrect login or password');
            // FOR DEVELOPMENT WITHOUT API!
            this.router.navigate(['/welcome']);

        }),
    );

    constructor(
        private actions$: Actions,
        private authService: AuthService,
        private localStorage: LocalStorageService,
        private notificationService: NotificationService,
        private router: Router,
    ) { }

}