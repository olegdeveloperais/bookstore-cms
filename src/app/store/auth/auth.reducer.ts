import { AuthActions, AuthTypes } from './auth.action';

export class AuthState {

}

export function reducer(state = new AuthState(), action: AuthActions): AuthState {
    switch (action.type) {
        default: {
            return state;
        }
    }
}
