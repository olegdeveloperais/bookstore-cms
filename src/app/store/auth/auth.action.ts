import { Action } from '@ngrx/store';
import { ICredentials } from '../../models/interfaces/credentials';

export enum AuthTypes {
    Login = '[AUTH] Log in [...]',
    LoginSuccess = '[AUTH] Log in [SUCCESS]',
    LoginError = '[AUTH] Log in [ERROR]',
    Logout = '[AUTH] Log out',
}

// LOG IN
export class LoginAction implements Action {
    readonly type = AuthTypes.Login;

    constructor(public payload: ICredentials) { }
}

export class LoginSuccessAction implements Action {
    readonly type = AuthTypes.LoginSuccess;

    constructor(public payload: string) { }
}

export class LoginErrorAction implements Action {
    readonly type = AuthTypes.LoginError;

    constructor(public payload: any) { }
}

// LOG OUT
export class LogoutAction implements Action {
    readonly type = AuthTypes.Logout;
}

export type AuthActions
    = LoginAction
    | LoginSuccessAction
    | LoginErrorAction
    | LogoutAction
    ;