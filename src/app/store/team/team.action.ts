import { Action } from '@ngrx/store';
import { IEmployeeInfo, IClientEmployeeInfo } from '../../models/interfaces/employee-info';

export enum TeamTypes {
    LoadTeam = '[ADMIN-TEAM] load team [...]',
    LoadTeamSuccess = '[ADMIN-TEAM] load team [SUCCESS]',
    LoadTeamError = '[ADMIN-TEAM] load team [ERROR]',

    AddEmployee = '[ADMIN-TEAM] add employee [...]',
    AddEmployeeSuccess = '[ADMIN-TEAM] add employee [SUCCESS]',
    AddEmployeeError = '[ADMIN-TEAM] add employee [ERROR]',

    EditEmployee = '[ADMIN-TEAM] edit employee [...]',
    EditEmployeeSuccess = '[ADMIN-TEAM] edit employee [SUCCESS]',
    EditEmployeeError = '[ADMIN-TEAM] edit employee [ERROR]',

    DeleteEmployee = '[ADMIN-TEAM] delete employee [...]',
    DeleteEmployeeSuccess = '[ADMIN-TEAM] delete employee [SUCCESS]',
    DeleteEmployeeError = '[ADMIN-TEAM] delete employee [ERROR]',

    OpenTeamSelector = '[ADMIN-TEAM] open team selector',
}

// LOAD TEAM
export class LoadTeamAction implements Action {
    readonly type = TeamTypes.LoadTeam;
}

export class LoadTeamSuccessAction implements Action {
    readonly type = TeamTypes.LoadTeamSuccess;
    constructor(public payload: IEmployeeInfo[]) { }
}

export class LoadTeamErrorAction implements Action {
    readonly type = TeamTypes.LoadTeamError;
    constructor(public payload: any) { }
}

// ADD EMPLOYEE
export class AddEmployeeAction implements Action {
    readonly type = TeamTypes.AddEmployee;
}

export class AddEmployeeSuccessAction implements Action {
    readonly type = TeamTypes.AddEmployeeSuccess;
    constructor(public payload: IEmployeeInfo) { }
}

export class AddEmployeeErrorAction implements Action {
    readonly type = TeamTypes.AddEmployeeError;
    constructor(public payload: any) { }
}

// EDIT EMPLOYEE
export class EditEmployeeAction implements Action {
    readonly type = TeamTypes.EditEmployee;
    constructor(public payload: IEmployeeInfo) { }
}

export class EditEmployeeSuccessAction implements Action {
    readonly type = TeamTypes.EditEmployeeSuccess;
    constructor(public payload: IEmployeeInfo) { }
}

export class EditEmployeeErrorAction implements Action {
    readonly type = TeamTypes.EditEmployeeError;
    constructor(public payload: any) { }
}

// DELETE EMPLOYEE
export class DeleteEmployeeAction implements Action {
    readonly type = TeamTypes.DeleteEmployee;
    constructor(public payload: string) { }
}

export class DeleteEmployeeSuccessAction implements Action {
    readonly type = TeamTypes.DeleteEmployeeSuccess;
    constructor(public payload: string) { }
}

export class DeleteEmployeeErrorAction implements Action {
    readonly type = TeamTypes.DeleteEmployeeError;
    constructor(public payload: any) { }
}

// OPEN SELECT EMPLOYEE DIALOG
export class OpenTeamSelectorAction implements Action {
    readonly type = TeamTypes.OpenTeamSelector;
    // payload as callback with selected employee
    constructor(public payload: (employee: IClientEmployeeInfo) => void) { }
}

export type TeamActions
    = LoadTeamAction
    | LoadTeamSuccessAction
    | LoadTeamErrorAction
    | AddEmployeeAction
    | AddEmployeeSuccessAction
    | AddEmployeeErrorAction
    | EditEmployeeAction
    | EditEmployeeSuccessAction
    | EditEmployeeErrorAction
    | DeleteEmployeeAction
    | DeleteEmployeeSuccessAction
    | DeleteEmployeeErrorAction
    | OpenTeamSelectorAction
    ;