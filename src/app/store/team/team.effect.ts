import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { MatDialog, MatDialogRef } from '@angular/material';
import { HttpService } from '../../services/http.service';
import { Store } from '@ngrx/store';
import { AppState } from '..';
import { ErrorService } from '../../services/error.service';
import { IEmployeeInfo, IEmployeeInfoValue, IClientEmployeeInfo } from '../../models/interfaces/employee-info';
import { Observable, of, empty } from 'rxjs';
import {
    TeamActions, LoadTeamAction,
    LoadTeamSuccessAction, TeamTypes,
    LoadTeamErrorAction, AddEmployeeAction,
    AddEmployeeSuccessAction,
    AddEmployeeErrorAction,
    EditEmployeeAction,
    EditEmployeeSuccessAction,
    EditEmployeeErrorAction,
    DeleteEmployeeAction,
    DeleteEmployeeSuccessAction,
    OpenTeamSelectorAction,
    DeleteEmployeeErrorAction,
} from './team.action';
import { switchMap, map, catchError, withLatestFrom } from 'rxjs/operators';
import { getTeamState } from './team.reducer';
import { ConfirmDialogComponent } from '../../core/modals/confirm-dialog/confirm-dialog.component';
import { TeamSelectorComponent } from '../../core/modals/team-selector/team-selector.component';
import { getTranslatorState } from '../translator/translator.reducer';
import { NotificationService } from '../../services/notification.service';

@Injectable()
export class TeamEffects {
    teamSelectorDialogRef: MatDialogRef<TeamSelectorComponent>;

    @Effect()
    loadTeam$: Observable<TeamActions> = this.actions$.pipe(
        ofType<LoadTeamAction>(TeamTypes.LoadTeam),
        switchMap(() => {
            return this.http.loadTeam().pipe(
                map(res => new LoadTeamSuccessAction(res)),
                catchError(err => of(new LoadTeamErrorAction(err))),
            );
        }),
    );

    @Effect()
    addEmployee$: Observable<TeamActions> = this.actions$.pipe(
        ofType<AddEmployeeAction>(TeamTypes.AddEmployee),
        switchMap(() => {
            return this.http.addEmployee().pipe(
                map(employee => new AddEmployeeSuccessAction(employee)),
                catchError(err => of(new AddEmployeeErrorAction(err))),
            );
        }),
    );

    @Effect()
    editEmployee$: Observable<TeamActions> = this.actions$.pipe(
        ofType<EditEmployeeAction>(TeamTypes.EditEmployee),
        map(action => action.payload),
        switchMap(employee => {
            if (!employee) return empty();
            return this.http.editEmployee(employee).pipe(
                map(() => new EditEmployeeSuccessAction(employee)),
                catchError(err => of(new EditEmployeeErrorAction(err))),
            );
        }),
    );

    @Effect()
    deleteEmployee$: Observable<TeamActions> = this.actions$.pipe(
        ofType<DeleteEmployeeAction>(TeamTypes.DeleteEmployee),
        map(action => action.payload),
        switchMap(id => {
            const dialogRef = this.dialog.open(ConfirmDialogComponent);
            dialogRef.componentInstance.title = 'Are you sure?';
            dialogRef.componentInstance.value = id;
            return dialogRef.afterClosed();
        }),
        switchMap(id => {
            if (!id) return empty();
            return this.http.deleteEmployee(id).pipe(
                map(() => new DeleteEmployeeSuccessAction(id)),
                catchError(err => of(new DeleteEmployeeErrorAction(err))),
            );
        }),
    );

    @Effect()
    openTeamSelector$: Observable<TeamActions> = this.actions$.pipe(
        ofType<OpenTeamSelectorAction>(TeamTypes.OpenTeamSelector),
        map(action => action.payload),
        withLatestFrom(
            this.store.select(getTeamState).pipe(map(s => s.team)),
            this.store.select(getTranslatorState).pipe(map(s => s.activeLanguage)),
        ),
        switchMap(([callback, team, activeLang]) => {
            if (!activeLang || !team) return;
            const activeLangTeam: IClientEmployeeInfo[] = team.map(t => {
                const val = t.values.find(v => v.lang === activeLang);
                if (!val) return null;
                return <IClientEmployeeInfo>{
                    _id: t._id,
                    description: val.description,
                    firstName: val.firstName,
                    lastName: val.lastName,
                    photo: t.photo,
                    position: val.position,
                };
            }).filter(e => e != null);
            if (!activeLangTeam || !activeLangTeam.length) {
                this.notificationService.snackbar('You must first add some team members');
                return empty();
            }
            this.teamSelectorDialogRef = this.dialog.open(
                TeamSelectorComponent,
                { data: activeLangTeam, panelClass: 'no-padding' },
            );
            return this.teamSelectorDialogRef.afterClosed().pipe(
                map((employee: IClientEmployeeInfo) => callback(employee)),
                switchMap(() => empty()),
            );
        }),
    );


    constructor(
        private actions$: Actions,
        private http: HttpService,
        private dialog: MatDialog,
        private notificationService: NotificationService,
        private store: Store<AppState>,
    ) { }

}