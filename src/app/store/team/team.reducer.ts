import { AppState } from '../index';
import { TeamActions, TeamTypes } from './team.action';
import { IEmployeeInfo } from '../../models/interfaces/employee-info';

export class TeamState {
    isTeamLoading: boolean;
    team: IEmployeeInfo[] = [];
}

export function reducer(state = new TeamState(), action: TeamActions): TeamState {
    switch (action.type) {
        // LOAD PROJECTS
        case TeamTypes.LoadTeam: {
            return {
                ...state,
                isTeamLoading: true,
            };
        }
        case TeamTypes.LoadTeamSuccess: {
            return {
                ...state,
                isTeamLoading: false,
                team: [...action.payload].reverse(),
            };
        }
        case TeamTypes.LoadTeamError: {
            return {
                ...state,
                isTeamLoading: false,
                team: [],
            };
        }
        // ADD PROJECT
        case TeamTypes.AddEmployeeSuccess: {
            return {
                ...state,
                team: [
                    action.payload,
                    ...state.team.filter(t => t._id !== action.payload._id),
                ],
            };
        }
        // DELETE PROJECT
        case TeamTypes.DeleteEmployeeSuccess: {
            return {
                ...state,
                team: state.team.filter(t => t._id !== action.payload),
            };
        }
        // EDIT PROJECT
        case TeamTypes.EditEmployeeSuccess: {
            return {
                ...state,
                team: state.team.map(t => t._id === action.payload._id ? action.payload : t),
            };
        }
        default: {
            return state;
        }
    }
}

export const getTeamState = (state: AppState) => <TeamState>state.team;