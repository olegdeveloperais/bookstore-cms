import { AppState } from '../index';
import { BooksActions, BookTypes } from './books.action';
import { IBook } from '../../models/interfaces/book';
import { IPaginator } from '../../models/interfaces/paginator';

export class BooksState {
    isBooksLoading: boolean;
    books: IBook[];
    booksCount: number;
    paginator: IPaginator = { size: 10, index: 0 };
    search: string = '';
}

export function reducer(state = new BooksState(), action: BooksActions): BooksState {
    switch (action.type) {
        // LOAD BOOKS
        case BookTypes.LoadBooks: {
            return {
                ...state,
                isBooksLoading: true,
            };
        }
        case BookTypes.LoadBooksSuccess: {
            return {
                ...state,
                isBooksLoading: false,
                books: action.payload.books,
                booksCount: action.payload.booksCount,
            };
        }
        case BookTypes.LoadBooksError: {
            return {
                ...state,
                isBooksLoading: false,
                books: [],
            };
        }
        // SET PAGINATOR
        case BookTypes.SetPaginator: {
            return {
                ...state,
                paginator: action.payload,
            };
        }
        // SET SEARCH
        case BookTypes.SetSearch: {
            return {
                ...state,
                search: action.payload,
            };
        }
        default: {
            return state;
        }
    }
}

export const getBooksState = (state: AppState) => <BooksState>state.books;