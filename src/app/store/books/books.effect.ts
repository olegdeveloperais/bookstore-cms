import { Injectable } from '@angular/core';
import { Actions, ofType, Effect } from '@ngrx/effects';
import { MatDialog, MatDialogRef } from '@angular/material';
import { HttpService } from '../../services/http.service';
import { Observable, of, empty } from 'rxjs';
import {
    BooksActions,
    BookTypes,
    LoadBooksAction,
    LoadBooksSuccessAction,
    LoadBooksErrorAction,
    AddBookAction,
    EditBookAction,
    EditBooksCollectionErrorAction,
    DeleteBookAction,
    // SetPaginatorAction,
} from './books.action';
import { switchMap, map, catchError, skipWhile, tap, withLatestFrom } from 'rxjs/operators';
import { ConfirmDialogComponent } from '../../core/modals/confirm-dialog/confirm-dialog.component';
import { AddBookComponent } from '../../core/modals/add-book/add-book.component';
import { NotificationService } from '../../services/notification.service';
import { IBook } from '../../models/interfaces/book';
import { Store } from '@ngrx/store';
import { AppState } from '..';
import { getBooksState } from './books.reducer';
import { IPaginator } from '../../models/interfaces/paginator';

@Injectable()
export class BooksEffects {

    confirmDialogRef: MatDialogRef<ConfirmDialogComponent>;

    @Effect()
    loadBooks$: Observable<BooksActions> = this.actions$.pipe(
        ofType<LoadBooksAction>(BookTypes.LoadBooks),
        withLatestFrom(this.store.select(getBooksState).pipe(map(s => s))),
        switchMap(([action, state]) => {
            return this.http.loadBooks({
                ...state.paginator,
                search: state.search,
            }).pipe(
                map(res => new LoadBooksSuccessAction(res)),
                catchError(err => of(new LoadBooksErrorAction(err))),
            );
        }),
    );

    @Effect()
    editBook$: Observable<BooksActions> = this.actions$.pipe(
        ofType<EditBookAction>(BookTypes.EditBook),
        map(action => action.payload),
        switchMap(project => {
            return this.http.editBook(project).pipe(
                map(() => new LoadBooksAction()),
                catchError(err => of(new EditBooksCollectionErrorAction(err))),
            );
        }),
    );

    @Effect()
    addBook$: Observable<BooksActions> = this.actions$.pipe(
        ofType<AddBookAction>(BookTypes.AddBook),
        switchMap(() => {
            this.confirmDialogRef = this.dialog.open(AddBookComponent);
            // this.confirmDialogRef.componentInstance.title = 'Are you sure?';
            // this.confirmDialogRef.componentInstance.value = id;
            return this.confirmDialogRef.afterClosed();
        }),
        switchMap((newBook) => {
            if (!newBook) return empty();
            return this.http.addBook(newBook).pipe(
                map(() => new LoadBooksAction()),
                catchError(err => of(new EditBooksCollectionErrorAction(err))),
            );
        }),
    );

    @Effect()
    deleteBook$: Observable<BooksActions> = this.actions$.pipe(
        ofType<DeleteBookAction>(BookTypes.DeleteBook),
        map(action => action.payload),
        switchMap(id => {
            this.confirmDialogRef = this.dialog.open(ConfirmDialogComponent);
            this.confirmDialogRef.componentInstance.title = 'Are you sure?';
            this.confirmDialogRef.componentInstance.value = id;
            return this.confirmDialogRef.afterClosed();
        }),
        switchMap(id => {
            if (!id) return empty();
            return this.http.deleteBook(id).pipe(
                map(() => new LoadBooksAction()),
                catchError(err => of(new EditBooksCollectionErrorAction(err))),
            );
        }),
    );

    @Effect()
    errors$: Observable<BooksActions> = this.actions$.pipe(
        ofType<EditBooksCollectionErrorAction>(BookTypes.EditBooksCollectionError),
        map(action => action.payload),
        switchMap(err => {
            console.log(err);
            return empty();
        }),
    );

    constructor(
        private actions$: Actions,
        private http: HttpService,
        private dialog: MatDialog,
        private store: Store<AppState>,
    ) { }

}