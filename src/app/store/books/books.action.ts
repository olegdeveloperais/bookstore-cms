import { Action } from '@ngrx/store';
import { IDictionaryItem } from '../../models/interfaces/dictionary-item';
import { IBook } from '../../models/interfaces/book';
import { IPaginator } from '../../models/interfaces/paginator';

export enum BookTypes {
    LoadBooks = '[ADMIN-BOOKS] load books [...]',
    LoadBooksSuccess = '[ADMIN-BOOKS] load books [SUCCESS]',
    LoadBooksError = '[ADMIN-BOOKS] load books [ERROR]',

    SetPaginator = '[ADMIN-BOOKS] set paginator [SUCCESS]',
    SetSearch = '[ADMIN-BOOKS] set search [SUCCESS]',

    AddBook = '[ADMIN-BOOKS] add book [...]',
    // AddBookSuccess = '[ADMIN-BOOKS] add book [SUCCESS]',
    // AddBookError = '[ADMIN-BOOKS] add book [ERROR]',

    EditBook = '[ADMIN-BOOKS] edit book [...]',
    // EditBookSuccess = '[ADMIN-BOOKS] edit book [SUCCESS]',
    EditBooksCollectionError = '[ADMIN-BOOKS] edit books collection [ERROR]',

    DeleteBook = '[ADMIN-BOOKS] delete book [...]',
    DeleteBookSuccess = '[ADMIN-BOOKS] delete book [SUCCESS]',
    DeleteBookError = '[ADMIN-BOOKS] delete book [ERROR]',
}

// LOAD BOOKS
export class LoadBooksAction implements Action {
    readonly type = BookTypes.LoadBooks;
    // constructor(public payload?: {search: string}) { }
}

export class LoadBooksSuccessAction implements Action {
    readonly type = BookTypes.LoadBooksSuccess;
    constructor(public payload: { books: IBook[], booksCount: number }) { }
}

export class LoadBooksErrorAction implements Action {
    readonly type = BookTypes.LoadBooksError;
    constructor(public payload: any) { }
}

// SET PAGINATOR
export class SetPaginatorAction implements Action {
    readonly type = BookTypes.SetPaginator;
    constructor(public payload: IPaginator) { }
}

// SET SEARCH
export class SetSearchAction implements Action {
    readonly type = BookTypes.SetSearch;
    constructor(public payload: string) { }
}

// ADD PROJECT
export class AddBookAction implements Action {
    readonly type = BookTypes.AddBook;
}

// export class AddBookSuccessAction implements Action {
//     readonly type = BookTypes.AddBookSuccess;
//     constructor(public payload: IBook) { }
// }

// export class AddBookErrorAction implements Action {
//     readonly type = BookTypes.AddBookError;
//     constructor(public payload: any) { }
// }

// EDIT PROJECT
export class EditBookAction implements Action {
    readonly type = BookTypes.EditBook;
    constructor(public payload: IBook) { }
}

// export class EditBookSuccessAction implements Action {
//     readonly type = BookTypes.EditBookSuccess;
//     constructor(public payload: IBook) { }
// }


// DELETE PROJECT
export class DeleteBookAction implements Action {
    readonly type = BookTypes.DeleteBook;
    constructor(public payload: string) { }
}

// ERROR ON ANY EDIT OF BOOKS COLLECTION
export class EditBooksCollectionErrorAction implements Action {
    readonly type = BookTypes.EditBooksCollectionError;
    constructor(public payload: any) { }
}

// export class DeleteBookSuccessAction implements Action {
//     readonly type = BookTypes.DeleteBookSuccess;
//     constructor(public payload: string) { }
// }

// export class DeleteBookErrorAction implements Action {
//     readonly type = BookTypes.DeleteBookError;
//     constructor(public payload: any) { }
// }

export type BooksActions
    = LoadBooksAction
    | LoadBooksSuccessAction
    | LoadBooksErrorAction
    | SetPaginatorAction
    | SetSearchAction
    | AddBookAction
    // | AddBookSuccessAction
    // | AddBookErrorAction
    | EditBookAction
    // | EditBookSuccessAction
    | DeleteBookAction
    | EditBooksCollectionErrorAction
    // | DeleteBookSuccessAction
    // | DeleteBookErrorAction
    ;