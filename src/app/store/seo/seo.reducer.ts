import { AppState } from '..';
import { StoreHelper } from '../../core/helpers/store.helper';
import { SEOActions, SEOTypes } from './seo.action';
import { ISEOSettings } from '../../models/interfaces/seo-settings';

export class SEOState {
    settings: ISEOSettings[] = [];
    isSEOSettingsLoading: boolean;
}

export function reducer(state = new SEOState(), action: SEOActions): SEOState {
    switch (action.type) {
        // LOAD MEDIA
        case SEOTypes.LoadSEOSettings:
            return {
                ...state,
                isSEOSettingsLoading: true,
            };
        case SEOTypes.LoadSEOSettingsSuccess:
            return {
                ...state,
                settings: action.payload,
                isSEOSettingsLoading: false,
            };
        case SEOTypes.LoadSEOSettingsError:
            return {
                ...state,
                settings: undefined,
                isSEOSettingsLoading: false,
            };
        // ADD MEDIA ITEM
        case SEOTypes.AddSEOSettingsSuccess:
            return {
                ...state,
                settings: [...state.settings, action.payload],
            };
        // EDIT MEDIA ITEM
        case SEOTypes.EditSEOSettingsSuccess:
            return {
                ...state,
                settings: StoreHelper.updateStoreArrayObject(
                    state.settings,
                    state.settings.findIndex(s => s._id === action.payload._id),
                    action.payload,
                ),
            };
        // DELETE MEDIA ITEM
        case SEOTypes.DeleteSEOSettingsSuccess:
            return {
                ...state,
                settings: StoreHelper.deleteStoreArrayItem(
                    state.settings,
                    state.settings.findIndex(s => s._id === action.payload),
                ) || [],
            };
        default: {
            return state;
        }
    }
}

export const getSEOState = (state: AppState) => <SEOState>state.seo;