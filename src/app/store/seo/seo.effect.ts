import { switchMap, map, tap, catchError, withLatestFrom } from 'rxjs/operators';
import { empty, of, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { MatDialog } from '@angular/material';
import { HttpService } from '../../services/http.service';
import { ErrorService } from '../../services/error.service';
import { ConfirmDialogComponent } from '../../core/modals/confirm-dialog/confirm-dialog.component';
import { Store } from '@ngrx/store';
import {
    AddSEOSettingsAction, AddSEOSettingsSuccessAction, AddSEOSettingsErrorAction, EditSEOSettingsAction,
    EditSEOSettingsSuccessAction, EditSEOSettingsErrorAction, DeleteSEOSettingsAction, DeleteSEOSettingsSuccessAction,
    DeleteSEOSettingsErrorAction,
} from './seo.action';
import {
    SEOActions, SEOTypes, LoadSEOSettingsAction, LoadSEOSettingsSuccessAction,
    LoadSEOSettingsErrorAction,
} from './seo.action';
import { AppState } from '..';
import { getTranslatorState } from '../translator/translator.reducer';
import { SEOManagementEditorComponent } from '../../pages/cms/pages/seo-management/editor/editor.component';
import { ISEOSettings } from '../../models/interfaces/seo-settings';

@Injectable()
export class SEOEffects {

    @Effect()
    loadSEOSettings$: Observable<SEOActions> = this.actions$.pipe(
        ofType<LoadSEOSettingsAction>(SEOTypes.LoadSEOSettings),
        switchMap(() => {
            return this.http.loadSEOSettings().pipe(
                map(settings => new LoadSEOSettingsSuccessAction(settings)),
                catchError(error => of(new LoadSEOSettingsErrorAction(error))),
            );
        }),
    );

    @Effect()
    addSEOSettings$: Observable<SEOActions> = this.actions$.pipe(
        ofType<AddSEOSettingsAction>(SEOTypes.AddSEOSettings),
        switchMap(() => {
            return this.http.addSEOSettings().pipe(
                map((settings) => new AddSEOSettingsSuccessAction(settings)),
                catchError(error => of(new AddSEOSettingsErrorAction(error))),
            );
        }),
    );

    @Effect()
    editSEOSettings$: Observable<SEOActions> = this.actions$.pipe(
        ofType<EditSEOSettingsAction>(SEOTypes.EditSEOSettings),
        map(action => action.payload),
        withLatestFrom(this.store.select(getTranslatorState).pipe(map(s => s.activeLanguage))),
        switchMap(([settings, lang]) => {
            const dialog = this.dialog.open(SEOManagementEditorComponent, { width: '60vw' });
            dialog.componentInstance.settings = settings;
            dialog.componentInstance.activeLanguage = lang;
            return dialog.afterClosed();
        }),
        switchMap((settings: ISEOSettings) => {
            if (!settings) return empty();
            return this.http.updateSEOSettings(settings).pipe(
                map(() => new EditSEOSettingsSuccessAction(settings)),
                catchError(error => of(new EditSEOSettingsErrorAction(error))),
            );
        }),
    );

    @Effect()
    deleteSEOSettings$: Observable<SEOActions> = this.actions$.pipe(
        ofType<DeleteSEOSettingsAction>(SEOTypes.DeleteSEOSettings),
        map(action => action.payload),
        switchMap(id => {
            const dialogRef = this.dialog.open(ConfirmDialogComponent);
            dialogRef.componentInstance.title = 'Are you sure?';
            dialogRef.componentInstance.value = id;
            return dialogRef.afterClosed();
        }),
        switchMap(id => {
            if (!id) return empty();
            return this.http.deleteSEOSettings(id).pipe(
                map(() => new DeleteSEOSettingsSuccessAction(id)),
                catchError(error => of(new DeleteSEOSettingsErrorAction(error))),
            );
        }),
    );

    @Effect({ dispatch: false })
    onError$: Observable<SEOActions> = this.actions$.pipe(
        ofType<AddSEOSettingsErrorAction
            | EditSEOSettingsErrorAction | DeleteSEOSettingsErrorAction>
            (SEOTypes.LoadSEOSettingsError, SEOTypes.AddSEOSettingsError,
            SEOTypes.LoadSEOSettingsError, SEOTypes.EditSEOSettingsError,
            SEOTypes.DeleteSEOSettingsError),
        map(action => action.payload),
        tap(e => {
            this.errorService.resolve(e);
        }),
    );

    constructor(
        private actions$: Actions,
        private http: HttpService,
        private dialog: MatDialog,
        private errorService: ErrorService,
        private store: Store<AppState>,
    ) { }

}
