import { Action } from '@ngrx/store';
import { ISEOSettings } from '../../models/interfaces/seo-settings';

export enum SEOTypes {

    LoadSEOSettings = '[SEO] load SEO settings [...]',
    LoadSEOSettingsSuccess = '[SEO] load SEO settings [SUCCESS]',
    LoadSEOSettingsError = '[SEO] load SEO settings [ERROR]',

    AddSEOSettings = '[SEO] add SEO settings [...]',
    AddSEOSettingsSuccess = '[SEO] add SEO settings [Success]',
    AddSEOSettingsError = '[SEO] add SEO settings [Error]',

    EditSEOSettings = '[SEO] edit SEO settings [...]',
    EditSEOSettingsSuccess = '[SEO] edit SEO settings [Success]',
    EditSEOSettingsError = '[SEO] edit SEO settings [Error]',

    DeleteSEOSettings = '[SEO] delete SEO settings [...]',
    DeleteSEOSettingsSuccess = '[SEO] delete SEO settings [Success]',
    DeleteSEOSettingsError = '[SEO] delete SEO settings [Error]',
}

// LOAD SEO SETTINGS
export class LoadSEOSettingsAction implements Action {
    readonly type = SEOTypes.LoadSEOSettings;
}

export class LoadSEOSettingsSuccessAction implements Action {
    readonly type = SEOTypes.LoadSEOSettingsSuccess;
    constructor(public payload: ISEOSettings[]) { }
}


export class LoadSEOSettingsErrorAction implements Action {
    readonly type = SEOTypes.LoadSEOSettingsError;
    constructor(public payload: any) { }
}

// ADD SEO SETTINGS
export class AddSEOSettingsAction implements Action {
    readonly type = SEOTypes.AddSEOSettings;
}

export class AddSEOSettingsSuccessAction implements Action {
    readonly type = SEOTypes.AddSEOSettingsSuccess;
    constructor(public payload: ISEOSettings) { }
}

export class AddSEOSettingsErrorAction implements Action {
    readonly type = SEOTypes.AddSEOSettingsError;
    constructor(public payload: any) { }
}

// EDIT SEO SETTINGS
export class EditSEOSettingsAction implements Action {
    readonly type = SEOTypes.EditSEOSettings;
    constructor(public payload: ISEOSettings) { }
}

export class EditSEOSettingsSuccessAction implements Action {
    readonly type = SEOTypes.EditSEOSettingsSuccess;
    constructor(public payload: ISEOSettings) { }
}

export class EditSEOSettingsErrorAction implements Action {
    readonly type = SEOTypes.EditSEOSettingsError;
    constructor(public payload: any) { }
}

// DELETE SEO SETTINGS
export class DeleteSEOSettingsAction implements Action {
    readonly type = SEOTypes.DeleteSEOSettings;
    constructor(public payload: string) { }
}

export class DeleteSEOSettingsSuccessAction implements Action {
    readonly type = SEOTypes.DeleteSEOSettingsSuccess;
    constructor(public payload: string) { }
}

export class DeleteSEOSettingsErrorAction implements Action {
    readonly type = SEOTypes.DeleteSEOSettingsError;
    constructor(public payload: any) { }
}

export type SEOActions
    = LoadSEOSettingsAction
    | LoadSEOSettingsSuccessAction
    | LoadSEOSettingsErrorAction
    | AddSEOSettingsAction
    | AddSEOSettingsSuccessAction
    | AddSEOSettingsErrorAction
    | EditSEOSettingsAction
    | EditSEOSettingsSuccessAction
    | EditSEOSettingsErrorAction
    | DeleteSEOSettingsAction
    | DeleteSEOSettingsSuccessAction
    | DeleteSEOSettingsErrorAction
    ;