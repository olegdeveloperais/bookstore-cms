import { ActionReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { MetaReducer } from '@ngrx/store';
import { ActionReducerMap } from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as auth from './auth/auth.reducer';
import * as translator from './translator/translator.reducer';
import * as media from './media/media.reducer';
import * as seo from './seo/seo.reducer';
import * as books from './books/books.reducer';
import * as team from './team/team.reducer';
import * as feedbacks from './feedbacks/feedbacks.reducer';
import * as technologies from './technologies/technologies.reducer';
import * as navigation from './navigation/navigation.reducer';

export interface AppState {
    auth: auth.AuthState;
    translator: translator.TranslatorState;
    media: media.MediaState;
    seo: seo.SEOState;
    books: books.BooksState;
    team: team.TeamState;
    feedbacks: feedbacks.FeedbacksState;
    technologies: technologies.TechnologiesState;
    navigation: navigation.NavigationState;
}

export const appReducers: ActionReducerMap<AppState> = {
    auth: auth.reducer,
    translator: translator.reducer,
    media: media.reducer,
    seo: seo.reducer,
    books: books.reducer,
    team: team.reducer,
    feedbacks: feedbacks.reducer,
    technologies: technologies.reducer,
    navigation: navigation.reducer,
};

export function logger(reducer: ActionReducer<AppState>): ActionReducer<AppState> {
    return function (state: AppState, action: any): AppState {
        console.log(state, action);
        return reducer(state, action);
    };
}

export const metaReducers: MetaReducer<AppState>[] = !environment.production
    ? [logger, storeFreeze]
    : [];
