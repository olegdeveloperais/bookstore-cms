import { StoreHelper } from '../../core/helpers/store.helper';
import { ITechnology } from '../../models/interfaces/technology';
import { TechnologiesActions, TechnologiesTypes } from './technologies.action';
import { AppState } from '../index';

export class TechnologiesState {
    technologies: ITechnology[] = [];
    isTechnologiesLoading: boolean;
}

export function reducer(state = new TechnologiesState(), action: TechnologiesActions): TechnologiesState {
    switch (action.type) {
        // LOAD TECHNOLOGIES
        case TechnologiesTypes.LoadTechnologies:
            return {
                ...state,
                isTechnologiesLoading: true,
            };
        case TechnologiesTypes.LoadTechnologiesSuccess:
            return {
                ...state,
                technologies: action.payload,
                isTechnologiesLoading: false,
            };
        case TechnologiesTypes.LoadTechnologiesError:
            return {
                ...state,
                technologies: [],
                isTechnologiesLoading: false,
            };
        // ADD TECHNOLOGY
        case TechnologiesTypes.AddTechnologySuccess:
            return {
                ...state,
                technologies: [action.payload, ...state.technologies],
            };
        // EDIT TECHNOLOGY
        case TechnologiesTypes.EditTechnology:
            return {
                ...state,
                technologies: StoreHelper.updateStoreArrayObject(
                    state.technologies,
                    state.technologies.findIndex(s => s._id === action.payload._id),
                    action.payload,
                ),
            };
        // DELETE TECHNOLOGY
        case TechnologiesTypes.DeleteTechnologySuccess:
            return {
                ...state,
                technologies: StoreHelper.deleteStoreArrayItem(
                    state.technologies,
                    state.technologies.findIndex(s => s._id === action.payload),
                ) || [],
            };
        default: {
            return state;
        }
    }
}

export const getTechnologiesState = (state: AppState) => <TechnologiesState>state.technologies;