import { switchMap, map, tap, catchError } from 'rxjs/operators';
import { of, Observable, empty } from 'rxjs';
import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { HttpService } from '../../services/http.service';
import { ErrorService } from '../../services/error.service';
import { ConfirmDialogComponent } from '../../core/modals/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import {
    TechnologiesActions, TechnologiesTypes, LoadTechnologiesAction,
    LoadTechnologiesSuccessAction, LoadTechnologiesErrorAction,
    AddTechnologyAction, AddTechnologySuccessAction, AddTechnologyErrorAction, EditTechnologyAction,
    EditTechnologySuccessAction, EditTechnologyErrorAction, DeleteTechnologyAction, DeleteTechnologySuccessAction,
    DeleteTechnologyErrorAction,
} from './technologies.action';

@Injectable()
export class TechnologiesEffects {

    @Effect()
    loadTechnologies$: Observable<TechnologiesActions> = this.actions$.pipe(
        ofType<LoadTechnologiesAction>(TechnologiesTypes.LoadTechnologies),
        switchMap(() => {
            return this.http.loadTechnologies().pipe(
                map(technologies => new LoadTechnologiesSuccessAction(technologies.reverse())),
                catchError(error => of(new LoadTechnologiesErrorAction(error))),
            );
        }),
    );

    @Effect()
    addTechnology$: Observable<TechnologiesActions> = this.actions$.pipe(
        ofType<AddTechnologyAction>(TechnologiesTypes.AddTechnology),
        switchMap(() => {
            return this.http.addTechnology().pipe(
                map((technology) => new AddTechnologySuccessAction(technology)),
                catchError(error => of(new AddTechnologyErrorAction(error))),
            );
        }),
    );

    @Effect()
    updateTechnology$: Observable<TechnologiesActions> = this.actions$.pipe(
        ofType<EditTechnologyAction>(TechnologiesTypes.EditTechnology),
        map(action => action.payload),
        switchMap(technology => {
            return this.http.updateTechnology(technology).pipe(
                map(() => new EditTechnologySuccessAction(technology)),
                catchError(error => of(new EditTechnologyErrorAction(error))),
            );
        }),
    );

    @Effect()
    deleteTechnology$: Observable<TechnologiesActions> = this.actions$.pipe(
        ofType<DeleteTechnologyAction>(TechnologiesTypes.DeleteTechnology),
        map(action => action.payload),
        switchMap(id => {
            const dialogRef = this.dialog.open(ConfirmDialogComponent);
            dialogRef.componentInstance.title = 'Are you sure?';
            dialogRef.componentInstance.value = id;
            return dialogRef.afterClosed();
        }),
        switchMap(id => {
            if (!id) return empty();
            return this.http.deleteTechnology(id).pipe(
                map(() => new DeleteTechnologySuccessAction(id)),
                catchError(error => of(new DeleteTechnologyErrorAction(error))),
            );
        }),
    );

    @Effect({ dispatch: false })
    onError$: Observable<TechnologiesActions> = this.actions$.pipe(
        ofType<LoadTechnologiesErrorAction | AddTechnologyErrorAction | EditTechnologyErrorAction
            | DeleteTechnologyErrorAction>
            (TechnologiesTypes.LoadTechnologiesError, TechnologiesTypes.AddTechnologyError,
            TechnologiesTypes.EditTechnologyError, TechnologiesTypes.DeleteTechnologyError),
        map(action => action.payload),
        tap(e => {
            this.errorService.resolve(e);
        }),
    );

    constructor(
        private actions$: Actions,
        private http: HttpService,
        private errorService: ErrorService,
        private dialog: MatDialog,
    ) { }

}
