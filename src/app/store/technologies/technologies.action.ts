import { Action } from '@ngrx/store';
import { ITechnology } from '../../models/interfaces/technology';

export enum TechnologiesTypes {

    LoadTechnologies = '[TECHNOLOGIES] load technologies [...]',
    LoadTechnologiesSuccess = '[TECHNOLOGIES] load Technologies [SUCCESS]',
    LoadTechnologiesError = '[TECHNOLOGIES] load Technologies [ERROR]',

    AddTechnology = '[TECHNOLOGIES] add technology [...]',
    AddTechnologySuccess = '[TECHNOLOGIES] add technology [Success]',
    AddTechnologyError = '[TECHNOLOGIES] add technology [Error]',

    EditTechnology = '[TECHNOLOGIES] edit technology [...]',
    EditTechnologySuccess = '[TECHNOLOGIES] edit Technology [Success]',
    EditTechnologyError = '[TECHNOLOGIES] edit Technology [Error]',

    DeleteTechnology = '[TECHNOLOGIES] delete technology [...]',
    DeleteTechnologySuccess = '[TECHNOLOGIES] delete technology [Success]',
    DeleteTechnologyError = '[TECHNOLOGIES] delete technology [Error]',
}

// LOAD TECHNOLOGIES
export class LoadTechnologiesAction implements Action {
    readonly type = TechnologiesTypes.LoadTechnologies;
}

export class LoadTechnologiesSuccessAction implements Action {
    readonly type = TechnologiesTypes.LoadTechnologiesSuccess;
    constructor(public payload: ITechnology[]) { }
}


export class LoadTechnologiesErrorAction implements Action {
    readonly type = TechnologiesTypes.LoadTechnologiesError;
    constructor(public payload: any) { }
}

// ADD TECHNOLOGY
export class AddTechnologyAction implements Action {
    readonly type = TechnologiesTypes.AddTechnology;
}

export class AddTechnologySuccessAction implements Action {
    readonly type = TechnologiesTypes.AddTechnologySuccess;
    constructor(public payload: ITechnology) { }
}

export class AddTechnologyErrorAction implements Action {
    readonly type = TechnologiesTypes.AddTechnologyError;
    constructor(public payload: any) { }
}

// EDIT TECHNOLOGY
export class EditTechnologyAction implements Action {
    readonly type = TechnologiesTypes.EditTechnology;
    constructor(public payload: ITechnology) { }
}

export class EditTechnologySuccessAction implements Action {
    readonly type = TechnologiesTypes.EditTechnologySuccess;
    constructor(public payload: ITechnology) { }
}

export class EditTechnologyErrorAction implements Action {
    readonly type = TechnologiesTypes.EditTechnologyError;
    constructor(public payload: any) { }
}

// DELETE TECHNOLOGY
export class DeleteTechnologyAction implements Action {
    readonly type = TechnologiesTypes.DeleteTechnology;
    constructor(public payload: string) { }
}

export class DeleteTechnologySuccessAction implements Action {
    readonly type = TechnologiesTypes.DeleteTechnologySuccess;
    constructor(public payload: string) { }
}

export class DeleteTechnologyErrorAction implements Action {
    readonly type = TechnologiesTypes.DeleteTechnologyError;
    constructor(public payload: any) { }
}

export type TechnologiesActions
    = LoadTechnologiesAction
    | LoadTechnologiesSuccessAction
    | LoadTechnologiesErrorAction
    | AddTechnologyAction
    | AddTechnologySuccessAction
    | AddTechnologyErrorAction
    | EditTechnologyAction
    | EditTechnologySuccessAction
    | EditTechnologyErrorAction
    | DeleteTechnologyAction
    | DeleteTechnologySuccessAction
    | DeleteTechnologyErrorAction
    ;