import { switchMap, map, tap, catchError, withLatestFrom } from 'rxjs/operators';
import { empty, of, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { MatDialog } from '@angular/material';
import { HttpService } from '../../services/http.service';
import { ErrorService } from '../../services/error.service';
import {
    AddMediaItemAction, AddMediaItemSuccessAction, AddMediaItemErrorAction, EditMediaItemAction,
    EditMediaItemSuccessAction, EditMediaItemErrorAction, DeleteMediaItemAction, DeleteMediaItemSuccessAction,
    DeleteMediaItemErrorAction,
} from './media.action';
import { DictionaryEditorComponent } from '../../core/modals/dictionary-editor/dictionary-editor.component';
import {
    MediaActions, MediaTypes, LoadMediaAction, LoadMediaSuccessAction,
    LoadMediaErrorAction,
} from './media.action';
import { IMediaItem } from '../../models/interfaces/media-item';
import { ConfirmDialogComponent } from '../../core/modals/confirm-dialog/confirm-dialog.component';

@Injectable()
export class MediaEffects {

    @Effect()
    loadMedia$: Observable<MediaActions> = this.actions$.pipe(
        ofType<LoadMediaAction>(MediaTypes.LoadMedia),
        switchMap(() => {
            return this.http.loadMedia().pipe(
                map(media => new LoadMediaSuccessAction(media)),
                catchError(error => of(new LoadMediaErrorAction(error))),
            );
        }),
    );

    @Effect()
    addMediaItem$: Observable<MediaActions> = this.actions$.pipe(
        ofType<AddMediaItemAction>(MediaTypes.AddMediaItem),
        switchMap(() => {
            return this.http.addMediaItem().pipe(
                map((res) => new AddMediaItemSuccessAction(res)),
                catchError(error => of(new AddMediaItemErrorAction(error))),
            );
        }),
    );

    @Effect()
    editMediaItem$: Observable<MediaActions> = this.actions$.pipe(
        ofType<EditMediaItemAction>(MediaTypes.EditMediaItem),
        map(action => action.payload),
        switchMap(item => {
            if (!item) return empty();
            return this.http.updateMediaItem(item).pipe(
                map(() => new EditMediaItemSuccessAction(item)),
                catchError(error => of(new EditMediaItemErrorAction(error))),
            );
        }),
    );

    @Effect()
    deleteMediaItem$: Observable<MediaActions> = this.actions$.pipe(
        ofType<DeleteMediaItemAction>(MediaTypes.DeleteMediaItem),
        map(action => action.payload),
        switchMap(id => {
            const dialogRef = this.dialog.open(ConfirmDialogComponent);
            dialogRef.componentInstance.title = 'Are you sure?';
            dialogRef.componentInstance.value = id;
            return dialogRef.afterClosed();
        }),
        switchMap(id => {
            if (!id) return empty();
            return this.http.deleteMediaItem(id).pipe(
                map(() => new DeleteMediaItemSuccessAction(id)),
                catchError(error => of(new DeleteMediaItemErrorAction(error))),
            );
        }),
    );

    @Effect({ dispatch: false })
    onError$: Observable<MediaActions> = this.actions$.pipe(
        ofType<AddMediaItemErrorAction
            | EditMediaItemErrorAction | DeleteMediaItemErrorAction>
            (MediaTypes.LoadMediaError, MediaTypes.AddMediaItemError,
            MediaTypes.LoadMediaError, MediaTypes.EditMediaItemError,
            MediaTypes.DeleteMediaItemError),
        map(action => action.payload),
        tap(e => {
            this.errorService.resolve(e);
        }),
    );

    constructor(
        private actions$: Actions,
        private http: HttpService,
        private dialog: MatDialog,
        private errorService: ErrorService,
    ) { }

}
