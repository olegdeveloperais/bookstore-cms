import { Action } from '@ngrx/store';
import { IMediaItem } from '../../models/interfaces/media-item';

export enum MediaTypes {
    LoadMedia = '[MEDIA] load media [...]',
    LoadMediaSuccess = '[MEDIA] load media [SUCCESS]',
    LoadMediaError = '[MEDIA] load media [ERROR]',

    AddMediaItem = '[MEDIA] add media item [...]',
    AddMediaItemSuccess = '[MEDIA] add media item [Success]',
    AddMediaItemError = '[MEDIA] add media item [Error]',

    EditMediaItem = '[MEDIA] edit media item [...]',
    EditMediaItemSuccess = '[MEDIA] edit media item [Success]',
    EditMediaItemError = '[MEDIA] edit media item [Error]',

    DeleteMediaItem = '[MEDIA] delete media item [...]',
    DeleteMediaItemSuccess = '[MEDIA] delete media item [Success]',
    DeleteMediaItemError = '[MEDIA] delete media item [Error]',
}

// LOAD MMEDIA
export class LoadMediaAction implements Action {
    readonly type = MediaTypes.LoadMedia;
}

export class LoadMediaSuccessAction implements Action {
    readonly type = MediaTypes.LoadMediaSuccess;
    constructor(public payload: IMediaItem[]) { }
}


export class LoadMediaErrorAction implements Action {
    readonly type = MediaTypes.LoadMediaError;
    constructor(public payload: any) { }
}

// ADD MMEDIA ITEM
export class AddMediaItemAction implements Action {
    readonly type = MediaTypes.AddMediaItem;
}

export class AddMediaItemSuccessAction implements Action {
    readonly type = MediaTypes.AddMediaItemSuccess;
    constructor(public payload: IMediaItem) { }
}

export class AddMediaItemErrorAction implements Action {
    readonly type = MediaTypes.AddMediaItemError;
    constructor(public payload: any) { }
}

// EDIT MMEDIA ITEM
export class EditMediaItemAction implements Action {
    readonly type = MediaTypes.EditMediaItem;
    constructor(public payload: IMediaItem) { }
}

export class EditMediaItemSuccessAction implements Action {
    readonly type = MediaTypes.EditMediaItemSuccess;
    constructor(public payload: IMediaItem) { }
}

export class EditMediaItemErrorAction implements Action {
    readonly type = MediaTypes.EditMediaItemError;
    constructor(public payload: any) { }
}

// DELETE MMEDIA ITEM
export class DeleteMediaItemAction implements Action {
    readonly type = MediaTypes.DeleteMediaItem;
    constructor(public payload: string) { }
}

export class DeleteMediaItemSuccessAction implements Action {
    readonly type = MediaTypes.DeleteMediaItemSuccess;
    constructor(public payload: string) { }
}

export class DeleteMediaItemErrorAction implements Action {
    readonly type = MediaTypes.DeleteMediaItemError;
    constructor(public payload: any) { }
}

export type MediaActions
    = LoadMediaAction
    | LoadMediaSuccessAction
    | LoadMediaErrorAction
    | AddMediaItemAction
    | AddMediaItemSuccessAction
    | AddMediaItemErrorAction
    | EditMediaItemAction
    | EditMediaItemSuccessAction
    | EditMediaItemErrorAction
    | DeleteMediaItemAction
    | DeleteMediaItemSuccessAction
    | DeleteMediaItemErrorAction
    ;