import { AppState } from '..';
import { StoreHelper } from '../../core/helpers/store.helper';
import { IMediaItem } from '../../models/interfaces/media-item';
import { MediaActions, MediaTypes } from './media.action';

export class MediaState {
    media: IMediaItem[];
    isMediaLoading: boolean;
}

export function reducer(state = new MediaState(), action: MediaActions): MediaState {
    switch (action.type) {
        // LOAD MEDIA
        case MediaTypes.LoadMedia:
            return {
                ...state,
                isMediaLoading: true,
            };
        case MediaTypes.LoadMediaSuccess:
            return {
                ...state,
                media: [...action.payload].reverse(),
                isMediaLoading: false,
            };
        case MediaTypes.LoadMediaError:
            return {
                ...state,
                media: undefined,
                isMediaLoading: false,
            };
        // ADD MEDIA ITEM
        case MediaTypes.AddMediaItemSuccess:
            return {
                ...state,
                media: [action.payload, ...state.media],
            };
        // EDIT MEDIA ITEM
        case MediaTypes.EditMediaItemSuccess:
            return {
                ...state,
                media: StoreHelper.updateStoreArrayObject(
                    state.media,
                    state.media.findIndex(s => s._id === action.payload._id),
                    action.payload,
                ),
            };
        // DELETE MEDIA ITEM
        case MediaTypes.DeleteMediaItemSuccess:
            return {
                ...state,
                media: StoreHelper.deleteStoreArrayItem(
                    state.media,
                    state.media.findIndex(s => s._id === action.payload),
                ),
            };
        default: {
            return state;
        }
    }
}

export const getMediaState = (state: AppState) => <MediaState>state.media;