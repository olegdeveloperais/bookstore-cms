import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { tap, withLatestFrom, map, switchMap } from 'rxjs/operators';
import { Observable, empty } from 'rxjs';
import { NavigationActions, NavigationTypes, ScrollTopAction } from './navigation.action';
import { Store } from '@ngrx/store';
import { AppState } from '..';
import { getNavigationState } from './navigation.reducer';

@Injectable()
export class NavigationEffects {

    @Effect({ dispatch: false })
    scrollTop$: Observable<NavigationActions> = this.actions$.pipe(
        ofType<ScrollTopAction>(NavigationTypes.ScrollTop),
        withLatestFrom(this.store$.select(getNavigationState).pipe(map(s => s.scrollContainer))),
        tap(([action, container]) => {
            container.scrollTop = 0;
        }),
        switchMap(() => empty()),
    );

    constructor(
        private actions$: Actions,
        private store$: Store<AppState>,
    ) { }

}