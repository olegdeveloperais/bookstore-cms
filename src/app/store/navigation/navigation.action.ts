import { Action } from '@ngrx/store';

export enum NavigationTypes {
    SetScrollContainer = '[NAVIGATION] Set scroll container',
    ScrollTop = '[NAVIGATION] Scroll top',
}

// SCROLL TOP
export class SetScrollContainerAction implements Action {
    readonly type = NavigationTypes.SetScrollContainer;
    constructor(public payload: HTMLElement) { }
}

// SCROLL TOP
export class ScrollTopAction implements Action {
    readonly type = NavigationTypes.ScrollTop;
}

export type NavigationActions
    = ScrollTopAction |
    SetScrollContainerAction
    ;