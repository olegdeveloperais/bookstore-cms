import { NavigationActions, NavigationTypes } from './navigation.action';
import { AppState } from '../index';

export class NavigationState {
    scrollContainer: HTMLElement;
}

export function reducer(state = new NavigationState(), action: NavigationActions): NavigationState {
    switch (action.type) {
        // SET SCROLL CONTAINER
        case NavigationTypes.SetScrollContainer:
            return {
                ...state,
                scrollContainer: action.payload,
            };
        default: {
            return state;
        }
    }
}

export const getNavigationState = (state: AppState) => <NavigationState>state.navigation;