import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginAction } from '../../store/auth/auth.action';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
    form: FormGroup;

    constructor(
        private store: Store<AppState>,
        private fb: FormBuilder,
    ) { }

    ngOnInit(): void {
        this.form = this.fb.group({
            username: ['', [Validators.required]],
            password: ['', [Validators.required]],
        });
    }
    onLogin(): void {
        if (this.form.invalid) return;
        const creds = this.form.value;
        this.store.dispatch(new LoginAction(creds));
    }
}
