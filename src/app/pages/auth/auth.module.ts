import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatToolbarModule, MatButtonModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { AUTH_ROUTES } from './auth.routing';

@NgModule({
    imports: [
        CommonModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatToolbarModule,
        MatButtonModule,
        RouterModule.forChild(AUTH_ROUTES),
    ],
    exports: [],
    declarations: [
        AuthComponent,
    ],
    providers: [

    ],
})
export class AuthModule { }
