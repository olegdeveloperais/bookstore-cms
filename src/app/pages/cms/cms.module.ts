import { NgModule } from '@angular/core';
import { CMSRootComponent } from './cms.component';
import { RouterModule } from '@angular/router';
import { CMS_ROUTES } from './cms.routing';
import { LayoutModule } from './layout/layout.module';
import { CMS_PAGES } from './pages';

@NgModule({
    imports: [
        RouterModule.forChild(CMS_ROUTES),
        LayoutModule,
        ...CMS_PAGES,
    ],
    declarations: [
        CMSRootComponent,
    ],
})
export class CMSModule { }
