
import { Route } from '@angular/router';
import { CMSGuard } from '../../core/guards/cms.guard';
import { CMSRootComponent } from './cms.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { BooksComponent } from './pages/books/books.component';
import { TeamComponent } from './pages/team/team.component';
import { FeedbackComponent } from './pages/feedback/feedback.component';
import { TranslatorComponent } from './pages/translator/translator.component';
import { SeoManagementComponent } from './pages/seo-management/seo-management.component';
import { MediaComponent } from './pages/media/media.component';
import { TechnologiesComponent } from './pages/technologies/technologies.component';

export const CMS_ROUTES: Route[] = [
    {
        path: '', component: CMSRootComponent, canActivate: [CMSGuard], children: [
            { path: '', redirectTo: 'welcome', pathMatch: 'full' },
            { path: 'welcome', component: WelcomeComponent },
            { path: 'translator', component: TranslatorComponent },
            { path: 'seo', component: SeoManagementComponent },
            { path: 'media', component: MediaComponent },
            { path: 'books', component: BooksComponent },
            { path: 'team', component: TeamComponent },
            { path: 'feedback', component: FeedbackComponent },
            { path: 'technologies', component: TechnologiesComponent },
        ],
    },
];



