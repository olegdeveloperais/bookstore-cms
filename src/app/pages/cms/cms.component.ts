import { Component, OnInit } from '@angular/core';
import { AppInitializerService } from '../../services/app-initializer.service';

@Component({
    selector: 'cms-root',
    templateUrl: 'cms.component.html',
})
export class CMSRootComponent implements OnInit {

    constructor(private appInitializer: AppInitializerService) { }

    ngOnInit(): void {
        this.appInitializer.init();
    }

}
