import { Component, OnInit, Input, Output, EventEmitter, OnChanges, OnDestroy } from '@angular/core';
import { ISEOSettings, ISEOSettingsValue, IOGSettings } from '../../../../../models/interfaces/seo-settings';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StoreHelper } from '../../../../../core/helpers/store.helper';
import { DocumentFileType } from '../../../../../models/enums/uploader-item';
import { MatDialogRef } from '@angular/material';
import { PageStatusCode } from '../../../../../models/enums/page-status-code';
import { Subscription } from 'rxjs/Subscription';
import { fadeInOut } from '../../../../../core/animations/fade.animation';

@Component({
    selector: 'seo-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss'],
    animations: [fadeInOut],
})
export class SEOManagementEditorComponent implements OnInit, OnDestroy {
    PageStatusCode = PageStatusCode;
    isWebtoolsVisible: boolean = false;
    codes = [
        PageStatusCode.OK,
    ];
    settings: ISEOSettings;
    activeLanguage: string;
    form: FormGroup;
    subs: Subscription = new Subscription();
    imgTypes: DocumentFileType[] = [
        DocumentFileType.JPEG, DocumentFileType.JPG, DocumentFileType.PNG,
    ];

    constructor(
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<SEOManagementEditorComponent>,
    ) { }

    ngOnInit(): void {
        if (!this.settings || !this.activeLanguage) return;
        this.initForm();
        if (this.settings.statusCode !== PageStatusCode.MOVED_PERMANENTLY) {
            this.form.controls.redirectUrl.disable();
        }
        this.subs.add(
            this.form.controls.statusCode.valueChanges.subscribe(v => {
                const ctrl = this.form.controls.redirectUrl;
                ctrl.setValue('');
                if (v === PageStatusCode.MOVED_PERMANENTLY) {
                    ctrl.enable();
                    ctrl.setValidators(Validators.required);
                } else {
                    ctrl.disable();
                }
                ctrl.markAsPristine();
                ctrl.markAsUntouched();
                ctrl.updateValueAndValidity();
            }),
        );
        if (this.settings.googleSearchConsole || this.settings.yandexWebmaster) this.isWebtoolsVisible = true;
    }

    ngOnDestroy(): void {
        this.subs.unsubscribe();
    }

    initForm(): void {
        const value: ISEOSettingsValue = this.settings.values.find(v => v.lang === this.activeLanguage);
        this.form = this.fb.group({
            label: [this.settings.label || '', Validators.required],
            url: [this.settings.url || '', Validators.required],
            statusCode: [this.settings.statusCode, Validators.required],
            redirectUrl: [this.settings.redirectUrl || ''],
            indexing: [this.settings.indexing],
            followingLinks: [this.settings.followingLinks],
            googleSearchConsole: [this.settings.googleSearchConsole || ''],
            yandexWebmaster: [this.settings.yandexWebmaster || ''],
            title: [value && value.og ? value.og.title : ''],
            keywords: [value && value.og ? value.og.keywords : ''],
            description: [value && value.og ? value.og.description : ''],
            image: [value && value.og ? value.og.image : ''],
        });
    }

    onUpdateSettings(): void {
        if (!this.form.valid) return;
        const v = this.form.value;
        const index = this.settings.values.findIndex(v => v.lang === this.activeLanguage);
        let settings = {
            ...this.settings,
            label: v.label,
            url: this.checkLeadingSlash(v.url),
            statusCode: v.statusCode,
            redirectUrl: v.redirectUrl,
            indexing: v.indexing,
            followingLinks: v.followingLinks,
        };
        if (v.googleSearchConsole) settings['googleSearchConsole'] = v.googleSearchConsole;
        if (v.yandexWebmaster) settings['yandexWebmaster'] = v.yandexWebmaster;

        const values = this.getValues(v);
        if (index > -1) {
            settings = values ? {
                ...settings,
                values: StoreHelper.updateStoreArrayObject(settings.values,
                    index,
                    values,
                ),
            } : settings;
        } else {
            settings = values ? {
                ...settings,
                values: [
                    ...settings.values,
                    values,
                ],
            } : settings;
        }
        this.dialogRef.close(settings);
    }

    getValues(v): ISEOSettingsValue {
        let res: any;
        if (this.checkNotEmptyFields(v)) res = {
            og: {
                title: v.title,
                keywords: v.keywords,
                description: v.description,
                image: v.image,
            },
        };
        if (!res) return;
        res = { ...res, lang: this.activeLanguage };
        return res;
    }

    checkNotEmptyFields(s: IOGSettings): boolean {
        return !!(s.description || s.image || s.keywords || s.title);
    }

    onReset(): void {
        this.ngOnInit();
    }

    onClose(): void {
        this.dialogRef.close();
    }

    clearWebtools(): void {
        this.form.controls.googleSearchConsole.setValue('');
        this.form.controls.yandexWebmaster.setValue('');
    }

    checkLeadingSlash(url: string): string {
        return url.substring(0, 1) !== '/' ? `/${url}` : url;
    }
}
