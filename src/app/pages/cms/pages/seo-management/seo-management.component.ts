import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ISEOSettings } from '../../../../models/interfaces/seo-settings';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/index';
import { getSEOState } from '../../../../store/seo/seo.reducer';
import { map } from 'rxjs/operators';
import { DeleteSEOSettingsAction, AddSEOSettingsAction, EditSEOSettingsAction } from '../../../../store/seo/seo.action';
import { getTranslatorState } from '../../../../store/translator/translator.reducer';
import { fadeIn } from '../../../../core/animations/fade.animation';
import { EnableTranslatorSelectorAction } from '../../../../store/translator/translator.action';
import { PageStatusCode } from '../../../../models/enums/page-status-code';

@Component({
    selector: 'seo-management',
    templateUrl: './seo-management.component.html',
    styleUrls: ['./seo-management.component.scss'],
    animations: [fadeIn],
})
export class SeoManagementComponent implements OnInit {
    PageStatusCode = PageStatusCode;
    isLoading$: Observable<boolean>;
    settings$: Observable<ISEOSettings[]>;
    activeLanguage$: Observable<string>;
    search: string;
    displayedColumns: string[] = ['status-icon', 'label', 'url', 'status', 'og',
        'web-tools', 'indexing', 'following', 'delete'];

    constructor(
        private store: Store<AppState>,
    ) { }

    ngOnInit(): void {
        this.store.dispatch(new EnableTranslatorSelectorAction(true));
        this.isLoading$ = this.store.select(getSEOState).pipe(map(s => s.isSEOSettingsLoading));
        this.settings$ = this.store.select(getSEOState).pipe(map(s => s.settings));
        this.activeLanguage$ = this.store.select(getTranslatorState).pipe(map(s => s.activeLanguage));
    }

    onCreateSettings(): void {
        this.store.dispatch(new AddSEOSettingsAction());
    }

    onDeleteSettings(id: string, e: MouseEvent): void {
        e.stopPropagation();
        e.preventDefault();
        this.store.dispatch(new DeleteSEOSettingsAction(id));
    }

    onUpdateSettings(settings: ISEOSettings): void {
        this.store.dispatch(new EditSEOSettingsAction(settings));
    }

    onSearch(val: string): void {
        this.search = val;
    }
}
