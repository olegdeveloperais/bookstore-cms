import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IProject } from '../../../../models/interfaces/project';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store';
import { map, tap, skipWhile, distinctUntilChanged } from 'rxjs/operators';
import {
    EditBookAction, AddBookAction,
    DeleteBookAction, LoadBooksAction, SetPaginatorAction, SetSearchAction,
} from '../../../../store/books/books.action';
import { fadeIn } from '../../../../core/animations/fade.animation';
import { getTranslatorState } from '../../../../store/translator/translator.reducer';
import { getBooksState, BooksState } from '../../../../store/books/books.reducer';
import { EnableTranslatorSelectorAction } from '../../../../store/translator/translator.action';
import { ITechnology } from '../../../../models/interfaces/technology';
import { IBook } from '../../../../models/interfaces/book';
import { PageEvent } from '@angular/material';

@Component({
    selector: 'books',
    templateUrl: './books.component.html',
    styleUrls: ['./books.component.scss'],
    animations: [fadeIn],
})
export class BooksComponent implements OnInit {

    activeLang$: Observable<string>;
    filter: string;
    isLoading$: Observable<boolean>;
    technologies$: Observable<ITechnology[]>;
    data$: Observable<BooksState>;

    // MatPaginator Inputs
    length = 1000;
    pageSize = 10;
    pageSizeOptions: number[] = [5, 10, 25, 100];

    // MatPaginator Output
    // pageEvent: PageEvent;
    // datasource = [];
    // data$;
    // activePageDataChunk = [];

    constructor(private store: Store<AppState>) { }

    ngOnInit(): void {
        // this.store.dispatch(new EnableTranslatorSelectorAction(true));
        // this.technologies$ = this.store.select(getTechnologiesState).pipe(map(s => s.technologies
        //     .filter(t => t.icon && t.label)));
        // this.activeLang$ = this.store.select(getTranslatorState).pipe(map(s => s.activeLanguage));
        // this.isLoading$ = this.store.select(getBooksState).pipe(map(s => s.isProjectsLoading));
        this.data$ = this.store.select(getBooksState).pipe(
            skipWhile(s => !s),
            map(s => s),
            // distinctUntilChanged(),
            // tap(books => this.datasource = books),
        );
    }

    createProject(): void {
        this.store.dispatch(new AddBookAction());
    }

    onProjectChanged(project: IBook): void {
        this.store.dispatch(new EditBookAction(project));
    }

    onDelete(id: string): void {
        this.store.dispatch(new DeleteBookAction(id));
    }

    // setPageSizeOptions(setPageSizeOptionsInput: string): void {
    //     this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    // }

    onPageChanged(e, paginator): void {
        console.log('onPageChanged: ', e, paginator);
        this.store.dispatch(new SetPaginatorAction({ index: e.pageIndex, size: e.pageSize }));
        this.store.dispatch(new LoadBooksAction());
    }

    onSearch(e): void {
        console.log(e.target.value);
        // this.store.dispatch(new LoadBooksAction({search: e.target.value}));
        this.store.dispatch(new SetSearchAction(e.target.value));
        this.store.dispatch(new LoadBooksAction());

    }

}
