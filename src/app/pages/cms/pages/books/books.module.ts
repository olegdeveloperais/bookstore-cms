import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BooksComponent } from './books.component';
import {
    MatIconModule, MatButtonModule,
    MatExpansionModule, MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatTabsModule,
    MatCardModule,
    MatSlideToggleModule,
    MatPaginatorModule,
} from '@angular/material';
// import { ProjectInfoComponent } from './project-info/project-info.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppCoreModule } from '../../../../core/core.module';
// import { PROJECT_INFO_FIELDS } from './project-info/fields';
import { ColorPickerModule } from 'ngx-color-picker';
import { BookInfoComponent } from './book-info/book-info.component';

@NgModule({
    declarations: [
        BooksComponent,
        BookInfoComponent,
        // ...PROJECT_INFO_FIELDS,
    ],
    imports: [
        CommonModule,
        AppCoreModule,
        MatIconModule,
        MatCheckboxModule,
        MatTooltipModule,
        MatButtonModule,
        MatInputModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatProgressBarModule,
        MatSlideToggleModule,
        MatSelectModule,
        MatExpansionModule,
        MatTabsModule,
        MatCardModule,
        FormsModule,
        ReactiveFormsModule,
        ColorPickerModule,
        MatPaginatorModule,
    ],
    exports: [
        BookInfoComponent,
        // ...PROJECT_INFO_FIELDS,
    ],
    providers: [],
})
export class BooksModule { }