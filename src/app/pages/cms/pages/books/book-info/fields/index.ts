import { ResultsFieldComponent } from './results-field/results-field.component';
import { ScopeFieldComponent } from './scope-field/scope-field.component';
import { MapFieldComponent } from './map-field/map-field.component';
import { GalleryFieldComponent } from './gallery-field/gallery-field.component';
import { TestimonialFieldComponent } from './testimonial-field/testimonial-field.component';

export const PROJECT_INFO_FIELDS = [
    ScopeFieldComponent,
    MapFieldComponent,
    ResultsFieldComponent,
    GalleryFieldComponent,
    TestimonialFieldComponent,
];