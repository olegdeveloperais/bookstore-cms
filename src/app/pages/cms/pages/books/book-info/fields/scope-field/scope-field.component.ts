import { Component, OnInit, forwardRef, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { IScope } from '../../../../../../../models/interfaces/project';
import { Subscription } from 'rxjs';
import { DocumentFileType } from '../../../../../../../models/enums/uploader-item';
import { NotificationService } from '../../../../../../../services/notification.service';

@Component({
    selector: 'scope-field',
    templateUrl: './scope-field.component.html',
    styleUrls: ['./scope-field.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ScopeFieldComponent),
            multi: true,
        },
    ],
})
export class ScopeFieldComponent implements OnInit, ControlValueAccessor, OnDestroy {
    currentSlide: number = 0;
    slidesCount: number;
    disabled: boolean = false;
    form: FormGroup;
    sub: Subscription;
    imgTypes: DocumentFileType[] = [
        DocumentFileType.JPEG, DocumentFileType.JPG, DocumentFileType.PNG,
    ];

    constructor(
        private fb: FormBuilder,
        private notificationService: NotificationService,
    ) { }

    ngOnInit(): void {
        this.initForm();
    }

    ngOnDestroy(): void {
        if (this.sub) this.sub.unsubscribe();
    }

    initForm(): void {
        this.form = this.fb.group({
            text: '',
            images: [[]],
        });
        this.sub = this.form.valueChanges.subscribe(() => {
            this.onChange(<IScope>this.form.value);
        });
    }

    uploadScope(path): void {
        if (this.form.get('images').value.length == 3) {
            this.notificationService.snackbar('Maximum number of pictures - 3');
            return;
        }
        const images = [...this.form.get('images').value, path];
        this.form.controls['images'].setValue(images);
        this.slidesCount++;
        this.currentSlide = this.slidesCount - 1;
    }

    removeGalleryImage(index: number): void {
        const gallery = [...this.form.get('images').value];
        gallery.splice(index, 1);
        this.slidesCount--;
        this.form.controls['images'].setValue(gallery);
    }

    slideChanged(index: number): void {
        this.currentSlide = index;
    }

    swipe(event): void {
        if (event.type === 'swipeleft') {
            this.nextSlide();
        }

        if (event.type === 'swiperight') {
            this.prevSlide();
        }
    }

    prevSlide(): void {
        const prevIndex = this.currentSlide - 1;
        this.currentSlide = prevIndex >= 0 ? prevIndex : this.slidesCount - 1;
    }

    nextSlide(): void {
        const nextIndex = this.currentSlide + 1;
        this.currentSlide = nextIndex < this.slidesCount ? nextIndex : 0;
    }

    indexTracker(index: number, value: any): number {
        return index;
    }

    writeValue(scope: IScope): void {
        if (!scope) {
            this.slidesCount = 0;
            return;
        }
        this.form.controls['text'].setValue(scope.text);
        this.form.controls['images'].setValue(scope.images);
        this.slidesCount = this.form.get('images').value.length || 0;
    }
    registerOnChange(fn: any): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }
    setDisabledState?(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    onChange: any = () => { };
    onTouched: any = () => { };

}
