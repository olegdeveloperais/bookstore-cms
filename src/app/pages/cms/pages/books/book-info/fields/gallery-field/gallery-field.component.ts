import { Component, OnInit, forwardRef, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { IScope } from '../../../../../../../models/interfaces/project';
import { Subscription } from 'rxjs';
import { DocumentFileType } from '../../../../../../../models/enums/uploader-item';

@Component({
    selector: 'gallery-field',
    templateUrl: './gallery-field.component.html',
    styleUrls: ['./gallery-field.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => GalleryFieldComponent),
            multi: true,
        },
    ],
})

export class GalleryFieldComponent implements OnInit, ControlValueAccessor, OnDestroy {
    currentSlide: number = 0;
    slidesCount: number;
    disabled: boolean = false;
    form: FormGroup;
    sub: Subscription;
    imgTypes: DocumentFileType[] = [
        DocumentFileType.JPEG, DocumentFileType.JPG, DocumentFileType.PNG,
    ];

    constructor(
        private fb: FormBuilder,
    ) { }

    ngOnInit(): void {
        this.initForm();
    }

    ngOnDestroy(): void {
        if (this.sub) this.sub.unsubscribe();
    }

    initForm(): void {
        this.form = this.fb.group({
            images: [[]],
        });
        this.sub = this.form.valueChanges.subscribe(() => {
            this.onChange(<IScope>this.form.value);
        });
    }

    uploadGallery(path): void {
        const images = [...this.form.get('images').value, path];
        this.form.controls['images'].setValue(images);
        this.slidesCount++;
        this.currentSlide = this.slidesCount - 1;
        this.onChange(this.form.value.images);
    }

    removeGalleryImage(index: number): void {
        const gallery = [...this.form.get('images').value];
        gallery.splice(index, 1);
        this.slidesCount--;
        this.form.controls['images'].setValue(gallery);
        this.onChange(this.form.value.images);
    }

    slideChanged(index: number): void {
        this.currentSlide = index;
    }

    swipe(event): void {
        if (event.type === 'swipeleft') {
            this.nextSlide();
        }

        if (event.type === 'swiperight') {
            this.prevSlide();
        }
    }

    prevSlide(): void {
        const prevIndex = this.currentSlide - 1;
        this.currentSlide = prevIndex >= 0 ? prevIndex : this.slidesCount - 1;
    }

    nextSlide(): void {
        const nextIndex = this.currentSlide + 1;
        this.currentSlide = nextIndex < this.slidesCount ? nextIndex : 0;
    }

    indexTracker(index: number, value: any): number {
        return index;
    }

    writeValue(arr: string[]): void {
        if (!arr) {
            this.slidesCount = 0;
            return;
        }
        this.form.controls['images'].setValue(arr);
        this.slidesCount = this.form.get('images').value.length || 0;
    }
    registerOnChange(fn: any): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState?(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    onChange: any = () => { };
    onTouched: any = () => { };

}
