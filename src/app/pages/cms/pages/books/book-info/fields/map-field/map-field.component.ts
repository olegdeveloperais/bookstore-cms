import { Component, Input, forwardRef, ViewChild, ElementRef, Output, EventEmitter, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ICoords } from '../../../../../../../models/interfaces/coords';

@Component({
    selector: 'map-field',
    templateUrl: './map-field.component.html',
    styleUrls: ['./map-field.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => MapFieldComponent),
            multi: true,
        },
    ],
})
export class MapFieldComponent implements ControlValueAccessor, OnInit {

    coords: ICoords;
    isPinAnim: boolean = false;
    disabled: boolean;
    @Output() unpin = new EventEmitter<any>();
    @Output() pin = new EventEmitter<ICoords>();
    @Input() mapImage: string = '/assets/img/map.svg';
    @ViewChild('map') map: ElementRef;

    ngOnInit(): void {
        if (this.map) {
            (<HTMLElement>this.map.nativeElement).ondragstart = () => false;
        }
    }

    onClick(event: MouseEvent): void {
        if (!this.map || this.disabled) return;
        const mapHeight: number = this.map.nativeElement.clientHeight;
        const mapWidth: number = this.map.nativeElement.clientWidth;
        if (mapHeight * mapWidth === 0) return;

        const coords: ICoords = {
            x: event.offsetX * 100 / mapWidth,
            y: event.offsetY * 100 / mapHeight,
        };
        this.writeValue(coords);
        this.onChange(coords);
        this.pin.emit(coords);
        this.isPinAnim = true;
        setTimeout(() => {
            this.isPinAnim = false;
        }, 500);
    }

    writeValue(coords: ICoords): void {
        this.coords = coords;
    }
    registerOnChange(fn: any): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }
    setDisabledState?(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    onChange: any = () => { };
    onTouched: any = () => { };

}
