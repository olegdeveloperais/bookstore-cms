import { Component, OnInit, forwardRef, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ITestimonial } from '../../../../../../../models/interfaces/testimonials';
import { DocumentFileType } from '../../../../../../../models/enums/uploader-item';
import { Subscription } from 'rxjs';
import { UploadButtonComponent } from '../../../../../../../core/components/upload-button/upload-button.component';

@Component({
    selector: 'testimonial-field',
    templateUrl: './testimonial-field.component.html',
    styleUrls: ['./testimonial-field.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => TestimonialFieldComponent),
            multi: true,
        },
    ],
})
export class TestimonialFieldComponent implements OnInit, ControlValueAccessor, OnDestroy {

    imgTypes: DocumentFileType[] = [
        DocumentFileType.JPEG, DocumentFileType.JPG, DocumentFileType.PNG,
    ];
    form: FormGroup;
    sub: Subscription;

    constructor(private fb: FormBuilder) { }

    ngOnInit(): void {
        this.initForm();
    }

    ngOnDestroy(): void {
        if (this.sub) this.sub.unsubscribe();
    }

    initForm(): void {
        this.form = this.fb.group({
            photo: ['', Validators.required],
            name: ['', Validators.required],
            position: [''],
            comment: ['', Validators.required],
            showOnSite: [false, Validators.required],
        });
        this.sub = this.form.valueChanges.subscribe(s => {
            this.onChange(this.form.value);
        });
    }

    uploadImage(path): void {
        this.form.controls['photo'].setValue(path);
    }

    writeValue(value: ITestimonial): void {
        if (!value) return;
        this.form.setValue(value);
    }
    registerOnChange(fn: any): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    onChange: any = () => { };
    onTouched: any = () => { };
}
