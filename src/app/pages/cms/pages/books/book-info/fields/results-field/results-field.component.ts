import { Component, OnInit, forwardRef, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { IProjectResult, IProject } from '../../../../../../../models/interfaces/project';
import { Subscription } from 'rxjs';

@Component({
    selector: 'results-field',
    templateUrl: './results-field.component.html',
    styleUrls: ['./results-field.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ResultsFieldComponent),
            multi: true,
        },
    ],
})
export class ResultsFieldComponent implements OnInit, ControlValueAccessor, OnDestroy {

    disabled: boolean = false;
    form: FormGroup;
    sub: Subscription;

    constructor(private fb: FormBuilder) { }

    ngOnInit(): void {
        this.initForm();
    }

    ngOnDestroy(): void {
        if (this.sub) this.sub.unsubscribe();
    }

    initForm(): void {
        this.form = this.fb.group({
            team: '',
            text: '',
            timeStart: '',
            timeEnd: '',
        });
        this.sub = this.form.valueChanges.subscribe(() => {
            this.onChange(<IProjectResult>this.form.value);
        });
    }

    writeValue(res: IProjectResult): void {
        if (!res) return;
        this.form.controls['team'].setValue(res.team);
        this.form.controls['text'].setValue(res.text);
        this.form.controls['timeStart'].setValue(res.timeStart);
        this.form.controls['timeEnd'].setValue(res.timeEnd);
    }
    registerOnChange(fn: any): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }
    setDisabledState?(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    onChange: any = () => { };
    onTouched: any = () => { };

}
