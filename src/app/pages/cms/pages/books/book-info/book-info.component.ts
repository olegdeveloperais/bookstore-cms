import { Component, OnChanges, Input, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core';
import { IProject, IProjectValue, IClientProject } from '../../../../../models/interfaces/project';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WebSiteValidator } from '../../../../../core/validators/custom.validators';
import { ProjectCategory } from '../../../../../models/enums/project-category';
import { MatExpansionPanel, MatCheckboxChange } from '@angular/material';
import { Store, ActionsSubject } from '@ngrx/store';
import { AppState } from '../../../../../store';
import { OpenTeamSelectorAction } from '../../../../../store/team/team.action';
import { StoreHelper } from '../../../../../core/helpers/store.helper';
import { NotificationService } from '../../../../../services/notification.service';
import { DocumentFileType } from '../../../../../models/enums/uploader-item';
import { Observable } from 'rxjs';
import { ITechnology } from '../../../../../models/interfaces/technology';
import { BookTypes } from '../../../../../store/books/books.action';
import { IBook } from '../../../../../models/interfaces/book';

@Component({
    selector: 'book-info',
    templateUrl: './book-info.component.html',
    styleUrls: ['./book-info.component.scss'],
})
export class BookInfoComponent implements OnChanges {
    imgTypes: DocumentFileType[] = [
        DocumentFileType.JPEG, DocumentFileType.JPG, DocumentFileType.PNG,
    ];
    showOnSite: boolean;
    categories: ProjectCategory[] = [
        ProjectCategory.Data,
        ProjectCategory.Ecommerce,
        ProjectCategory.Enterprise,
        ProjectCategory.Healthcare,
        ProjectCategory.Information,
        ProjectCategory.Blockchain,
        ProjectCategory.Booking,
    ];
    currentSlide: number = 0;
    slidesCount: number;
    form: FormGroup;
    activeProjectValue: IProjectValue;
    previewUrl: string;
    @Input() technologies: ITechnology[];
    @Input() data: IBook;
    @Input() activeLang: string;
    @Output() update = new EventEmitter<IProject>();
    @Output() delete = new EventEmitter<string>();
    @ViewChild(MatExpansionPanel) panel: MatExpansionPanel;

    constructor(
        private fb: FormBuilder,
        private notificationService: NotificationService,
        // private dispatcher: ActionsSubject,
    ) {
        // this.dispatcher.subscribe(action => {
        //     if (action.type === BookTypes.LoadBooksSuccess) {
        //         this.close();
        //     }
        // });
    }

    ngOnChanges(): void {
        if (!this.data) return;
        this.initForm(this.data);
    }

    initForm(book?: IBook): void {
        // const value: IProjectValue = this.activeProjectValue = project.values.find(p => p.lang === this.activeLang);
        // if (!value) {
            this.previewUrl = `http://localhost:3000/public/${book.img}`;

        this.form = this.fb.group({
            _id: book._id,
            author: book.author,
            title: book.title,
            description: book.description,
            price: book.price,
            img: book.img,
        });
}

    onSubmit(): void {
        console.log('onSubmit');

        if (!this.form.get('img').value) {
            this.notificationService.snackbar('Please add preview image');
            return;
        }
        if (this.form.invalid) {
            this.notificationService.snackbar('Please, fill in required fields');
            return;
        }
        this.update.emit(this.form.value);
    }

    onReset(): void {
        console.log('onReset');
        this.ngOnChanges();

    }

    close(): void {
        if (!this.panel) return;
        this.panel.close();
    }

    // onColorChanged(color: string): void {
    //     this.form.controls['projectColor'].setValue(color);
    //     this.form.markAsDirty();
    // }

    // resetForm(): void {
    //     this.ngOnChanges();
    // }

    onFileChanged(e): void {
        console.log('ONFILECHANGED', e);
        this.previewUrl = `http://localhost:3000/public/${e}`;
        this.form.controls.img.patchValue(e);
        console.log('form: ', this.form);

    }

    stopProp(event: Event): void {
        event.stopPropagation();
        // this.form.markAsDirty();
        // if (this.form.invalid) {
        //     this.notificationService.snackbar('You can\'t enable project visibility until fill in required fields');
        //     event.preventDefault();
        // }
    }

    // emitValue(): void {
    //     const v: IClientProject = this.form.value;
    //     const index = this.data.values.findIndex(v => v.lang === this.activeLang);
    //     let project = <IBook>{
    //         // ...this.data,
    //         // previewImage: v.previewImage || '',
    //         // projectColor: v.projectColor,
    //         // categories: v.categories || [],
    //         // images: v.images || [],
    //         // showOnSite: v.showOnSite,
    //         // website: v.website || '',
    //         // coords: v.coords,
    //         // technologies: v.technologies || [],
    //         // logo: v.logo || '',
    //         // name: v.name,
    //         _id: 'string',
    //         title: 'string',
    //         author: 'string',
    //         description: 'string',
    //         price: 9999,
    //     };
    //     if (index > -1) {
    //         project = {
    //             ...project,
    //             values: StoreHelper.updateStoreArrayObject(project.values,
    //                 index,
    //                 <IProjectValue>{
    //                     // lang: this.activeLang,
    //                     // aboutProject: v.aboutProject,
    //                     // excerpt: v.excerpt,
    //                     // result: v.result,
    //                     // scope: v.scope,
    //                     // testimonial: v.testimonial,
    //                 },
    //             ),
    //         };
    //     } else {
    //         project = {
    //             ...project,
    //             values: [
    //                 ...project.values,
    //                 <IProjectValue>{
    //                     lang: this.activeLang,
    //                     aboutProject: v.aboutProject,
    //                     excerpt: v.excerpt,
    //                     result: v.result,
    //                     scope: v.scope,
    //                     testimonial: v.testimonial,
    //                 },
    //             ],
    //         };
    //     }
    //     if (!project.coords) delete project.coords;
    // this.update.emit(project);
    // }

    onDelete(event: Event): void {
        event.stopPropagation();
        this.delete.emit(this.data._id);
    }

    toggleShowOnSite(event: MatCheckboxChange): void {
        this.showOnSite = event.checked;
        // this.form.controls['showOnSite'].setValue(this.showOnSite);
        // this.emitValue();
    }

    // onLogoChanged(path): void {
    //     this.form.controls['logo'].setValue(path);
    //     this.form.markAsDirty();
    // }

    // unpin(): void {
    //     this.form.controls['coords'].setValue(undefined);
    // }

    // uploadGallery(path): void {
    //     const images = [...this.form.get('images').value, path];
    //     this.form.controls['images'].setValue(images);
    //     console.log(this.form.get('images').value);

    //     this.slidesCount++;
    //     this.currentSlide = this.slidesCount - 1;
    // }

    // removeGalleryImage(index: number): void {
    //     const gallery = [...this.form.get('images').value];
    //     gallery.splice(index, 1);
    //     this.slidesCount--;
    //     this.form.controls['images'].setValue(gallery);

    // }

    // slideChanged(index: number): void {
    //     this.currentSlide = index;
    // }

    // swipe(event): void {
    //     if (event.type === 'swipeleft') {
    //         this.nextSlide();
    //     }

    //     if (event.type === 'swiperight') {
    //         this.prevSlide();
    //     }
    // }

    // prevSlide(): void {
    //     const prevIndex = this.currentSlide - 1;
    //     this.currentSlide = prevIndex >= 0 ? prevIndex : this.slidesCount - 1;
    // }

    // nextSlide(): void {
    //     const nextIndex = this.currentSlide + 1;
    //     this.currentSlide = nextIndex < this.slidesCount ? nextIndex : 0;
    // }
    // ngOnDestroy(): void {
        // console.log('onDESTROY', this.dispatcher);
        // this.dispatcher.unsubscribe();
        // console.log(this.dispatcher);
    // }
}
