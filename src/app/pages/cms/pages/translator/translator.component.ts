import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IDictionaryItem } from '../../../../models/interfaces/dictionary-item';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store';
import {
    AddDictionaryItemAction, DeleteDictionaryItemAction,
    EditDictionaryItemAction,
    EnableTranslatorSelectorAction,
} from '../../../../store/translator/translator.action';
import { getTranslatorState } from '../../../../store/translator/translator.reducer';
import { map, skipWhile } from 'rxjs/operators';
import { AddLanguageAction, DeleteLanguageAction } from '../../../../store/translator/translator.action';
import { fadeIn } from '../../../../core/animations/fade.animation';

@Component({
    selector: 'translator',
    templateUrl: './translator.component.html',
    styleUrls: ['./translator.component.scss'],
    animations: [fadeIn],
})
export class TranslatorComponent implements OnInit {
    isLoading$: Observable<boolean>;
    dictionary$: Observable<IDictionaryItem[]>;
    activeLanguage$: Observable<string>;
    languages$: Observable<string[]>;
    displayedColumns: string[] = ['key', 'value', 'delete'];

    constructor(
        private store: Store<AppState>,
    ) { }

    ngOnInit(): void {
        this.store.dispatch(new EnableTranslatorSelectorAction(true));
        this.isLoading$ = this.store.select(getTranslatorState).pipe(map(s => s.isDictionaryLoading));
        this.dictionary$ = this.store.select(getTranslatorState).pipe(map(s => s.dictionary));
        this.languages$ = this.store.select(getTranslatorState).pipe(map(s => s.languages));
        this.activeLanguage$ = this.store.select(getTranslatorState).pipe(map(s => s.activeLanguage));
    }

    onCreateLanguage(): void {
        this.store.dispatch(new AddLanguageAction());
    }

    onDeleteLanguage(lang: string, index: number): void {
        if (!index) return;
        this.store.dispatch(new DeleteLanguageAction(lang));
    }

    onCreateItem(): void {
        this.store.dispatch(new AddDictionaryItemAction());
    }

    onDeleteItem(id: string, e: MouseEvent): void {
        e.preventDefault();
        e.stopPropagation();
        this.store.dispatch(new DeleteDictionaryItemAction(id));
    }

    onEditItem(dictionaryItem: IDictionaryItem): void {
        this.store.dispatch(new EditDictionaryItemAction(dictionaryItem));
    }

}
