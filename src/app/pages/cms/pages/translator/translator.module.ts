import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {
    MatIconModule, MatButtonModule, MatSelectModule, MatCardModule, MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatExpansionModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatProgressBarModule,
} from '@angular/material';
import { AppCoreModule } from '../../../../core/core.module';
import { TranslatorComponent } from './translator.component';

@NgModule({
    imports: [
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        AppCoreModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        MatProgressBarModule,
        MatSelectModule,
        MatCardModule,
        MatFormFieldModule,
        MatRadioModule,
        MatExpansionModule,
        MatTableModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
    ],
    declarations: [
        TranslatorComponent,
    ],
    exports: [
        TranslatorComponent,
    ],
})
export class TranslatorModule { }