import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {
    MatIconModule, MatButtonModule, MatSelectModule, MatCardModule, MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatExpansionModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatProgressBarModule,
} from '@angular/material';
import { AppCoreModule } from '../../../../core/core.module';
import { TechnologiesComponent } from './technologies.component';
import { TechnologiesEditorComponent } from './editor/editor.component';

@NgModule({
    imports: [
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        AppCoreModule,
        MatInputModule,
        MatIconModule,
        MatProgressBarModule,
        MatButtonModule,
        MatSelectModule,
        MatCardModule,
        MatFormFieldModule,
        MatRadioModule,
        MatExpansionModule,
        MatTableModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        MatCheckboxModule,
    ],
    declarations: [
        TechnologiesComponent,
        TechnologiesEditorComponent,
    ],
    exports: [
        TechnologiesComponent,
    ],
})
export class TechnologiesModule { }