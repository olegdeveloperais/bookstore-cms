import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store';
import { map } from 'rxjs/operators';
import { fadeIn } from '../../../../core/animations/fade.animation';
import { ITechnology } from '../../../../models/interfaces/technology';
import { getTechnologiesState } from '../../../../store/technologies/technologies.reducer';
import {
    AddTechnologyAction, DeleteTechnologyAction,
    EditTechnologyAction,
} from '../../../../store/technologies/technologies.action';
import { EnableTranslatorSelectorAction } from '../../../../store/translator/translator.action';

@Component({
    selector: 'technologies',
    templateUrl: './technologies.component.html',
    styleUrls: ['./technologies.component.scss'],
    animations: [fadeIn],
})
export class TechnologiesComponent implements OnInit {
    isLoading$: Observable<boolean>;
    technologies$: Observable<ITechnology[]>;

    constructor(
        private store: Store<AppState>,
    ) { }

    ngOnInit(): void {
        this.store.dispatch(new EnableTranslatorSelectorAction(false));
        this.isLoading$ = this.store.select(getTechnologiesState).pipe(map(s => s.isTechnologiesLoading));
        this.technologies$ = this.store.select(getTechnologiesState).pipe(map(s => s.technologies));
    }

    onCreateTechnology(): void {
        this.store.dispatch(new AddTechnologyAction());
    }

    onDeleteTechnology(id: string): void {
        this.store.dispatch(new DeleteTechnologyAction(id));
    }

    onUpdateTechnology(technology: ITechnology): void {
        this.store.dispatch(new EditTechnologyAction(technology));
    }
}
