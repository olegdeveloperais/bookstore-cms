import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ITechnology } from '../../../../../models/interfaces/technology';
import { TechnologyGroup } from '../../../../../models/enums/technology-group';
import { DocumentFileType } from '../../../../../models/enums/uploader-item';
import { fadeInOut } from '../../../../../core/animations/fade.animation';
import { NotificationService } from '../../../../../services/notification.service';

@Component({
    selector: 'technology-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss'],
    animations: [fadeInOut],
})
export class TechnologiesEditorComponent implements OnChanges {
    TechnologyGroup = TechnologyGroup;
    @Input() technology: ITechnology;
    @Output() delete: EventEmitter<string> = new EventEmitter();
    @Output() update: EventEmitter<ITechnology> = new EventEmitter();
    form: FormGroup;
    imgTypes: DocumentFileType[] = [
        DocumentFileType.JPEG, DocumentFileType.JPG, DocumentFileType.PNG,
    ];

    constructor(
        private fb: FormBuilder,
        private notificationService: NotificationService,
    ) { }

    ngOnChanges(): void {
        if (!this.technology) return;
        this.form = this.fb.group({
            icon: [this.technology.icon || '', Validators.required],
            label: [this.technology.label || '', Validators.required],
            group: [this.technology.group, Validators.required],
            showOnHomepage: [this.technology.showOnHomepage || false],
        });
    }

    async onUpdateTechnology(): Promise<void> {
        if (!this.form.get('icon').value) {
            this.notificationService.snackbar('Please add image');
            return;
        }
        if (this.form.get('group').value == -1) {
            this.form.controls['group'].setValue(undefined);
        }
        if (!this.form.valid) return;
        const technology = {
            _id: this.technology._id,
            ...this.form.value,
        };
        this.update.emit(technology);
    }

    get isAnyChanges(): boolean {
        if (!this.form || !this.technology) return false;
        const v = this.form.value;
        const technology = this.technology;
        return (technology.label !== v.label || technology.icon !== v.icon ||
            technology.group !== v.group || technology.showOnHomepage !== v.showOnHomepage);
    }

    onCancel(): void {
        this.ngOnChanges();
    }

    onFileSelected(icon: string): void {
        this.form.controls['icon'].setValue(icon);
        this.form.controls['icon'].markAsDirty();
        this.form.controls['icon'].updateValueAndValidity();
    }
}
