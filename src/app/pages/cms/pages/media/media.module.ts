import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {
    MatIconModule, MatButtonModule, MatSelectModule, MatCardModule, MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatExpansionModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatPaginatorModule,
} from '@angular/material';
import { AppCoreModule } from '../../../../core/core.module';
import { MediaComponent } from './media.component';
import { MediaItemComponent } from './media-item/media-item.component';

@NgModule({
    imports: [
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        AppCoreModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        MatSelectModule,
        MatCardModule,
        MatProgressBarModule,
        MatFormFieldModule,
        MatRadioModule,
        MatExpansionModule,
        MatTableModule,
        MatProgressSpinnerModule,
        MatPaginatorModule,
    ],
    declarations: [
        MediaComponent,
        MediaItemComponent,
    ],
    exports: [
        MediaComponent,
    ],
})
export class MediaModule { }