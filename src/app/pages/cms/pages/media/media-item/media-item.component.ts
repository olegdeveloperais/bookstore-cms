import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { DocumentFileType } from '../../../../../models/enums/uploader-item';
import { IMediaItem } from '../../../../../models/interfaces/media-item';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from '../../../../../services/notification.service';
import { fadeInOut } from '../../../../../core/animations/fade.animation';

@Component({
    selector: 'media-item',
    templateUrl: './media-item.component.html',
    styleUrls: ['./media-item.component.scss'],
    animations: [fadeInOut],
})
export class MediaItemComponent implements OnChanges {
    imgTypes: DocumentFileType[] = [
        DocumentFileType.JPEG, DocumentFileType.JPG, DocumentFileType.PNG,
    ];
    form: FormGroup;
    @Input() data: IMediaItem;
    @Output() update = new EventEmitter<IMediaItem>();
    @Output() delete = new EventEmitter<string>();

    constructor(
        private fb: FormBuilder,
        private notificationService: NotificationService,
    ) {
    }

    ngOnChanges(): void {
        if (!this.data) return;
        this.initForm(this.data);
    }

    initForm(media?: IMediaItem): void {
        this.form = this.fb.group({
            key: [media.key || '', Validators.required],
            value: [media.value || '', Validators.required],
            alt: [media.alt || '', Validators.required],
        });
    }

    onFileChanged(data: string): void {
        this.form.controls['value'].setValue(data);
        this.form.controls['value'].markAsDirty();
    }

    onSubmit(): void {
        if (!this.form.get('value').value) {
            this.notificationService.snackbar('Please add image');
            return;
        }
        if (this.form.invalid) return;
        const v: IMediaItem = this.form.value;
        const media: IMediaItem = {
            ...this.data,
            key: v.key,
            value: v.value,
            alt: v.alt,
        };
        this.update.emit(media);
    }

    onCancel(): void {
        this.ngOnChanges();
    }
}
