import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IMediaItem } from '../../../../models/interfaces/media-item';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/index';
import { getMediaState } from '../../../../store/media/media.reducer';
import { map } from 'rxjs/operators';
import { AddMediaItemAction, DeleteMediaItemAction, EditMediaItemAction } from '../../../../store/media/media.action';
import { fadeIn } from '../../../../core/animations/fade.animation';
import { EnableTranslatorSelectorAction } from '../../../../store/translator/translator.action';
import { ScrollTopAction } from '../../../../store/navigation/navigation.action';

@Component({
    selector: 'media',
    templateUrl: './media.component.html',
    styleUrls: ['./media.component.scss'],
    animations: [fadeIn],
})
export class MediaComponent implements OnInit {
    isLoading$: Observable<boolean>;
    search: string;
    media$: Observable<IMediaItem[]>;
    pageIndex: number = 0;
    pageSize = 12;

    constructor(
        private store: Store<AppState>,
    ) { }

    ngOnInit(): void {
        this.store.dispatch(new EnableTranslatorSelectorAction(false));
        this.isLoading$ = this.store.select(getMediaState).pipe(map(s => s.isMediaLoading));
        this.media$ = this.store.select(getMediaState).pipe(map(s => s.media));
    }

    onCreateItem(): void {
        this.store.dispatch(new AddMediaItemAction());
    }

    onDelete(id: string): void {
        this.store.dispatch(new DeleteMediaItemAction(id));
    }

    onUpdate(media: IMediaItem): void {
        this.store.dispatch(new EditMediaItemAction(media));
    }

    onSearch(val: string): void {
        this.search = val;
    }

    onChangePage(index: number): void {
        this.pageIndex = index;
        this.store.dispatch(new ScrollTopAction());
    }
}
