import { WelcomeModule } from './welcome/welcome.module';
import { BooksModule } from './books/books.module';
import { TeamModule } from './team/team.module';
import { FeedbackModule } from './feedback/feedback.module';
import { MediaModule } from './media/media.module';
import { SEOManagementModule } from './seo-management/seo-management.module';
import { TranslatorModule } from './translator/translator.module';
import { TechnologiesModule } from './technologies/technologies.module';

export const CMS_PAGES = [
    BooksModule,
    WelcomeModule,
    TeamModule,
    FeedbackModule,
    MediaModule,
    SEOManagementModule,
    TranslatorModule,
    TechnologiesModule,
];