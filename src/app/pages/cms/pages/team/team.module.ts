import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamComponent } from './team.component';
import {
    MatIconModule, MatButtonModule, MatExpansionModule,
    MatFormFieldModule, MatInputModule, MatProgressBarModule, MatSlideToggleModule, MatSelectModule,
} from '@angular/material';
import { TeamMemberComponent } from './team-member/team-member.component';
import { AppCoreModule } from '../../../../core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        TeamComponent,
        TeamMemberComponent,
    ],
    imports: [
        CommonModule,
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatExpansionModule,
        MatProgressBarModule,
        MatSelectModule,
        MatSlideToggleModule,
        AppCoreModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [
        TeamMemberComponent,
    ],
    providers: [],
})
export class TeamModule { }