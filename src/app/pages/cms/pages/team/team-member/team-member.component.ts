import { Component, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { IEmployeeInfo, IEmployeeInfoValue, IClientEmployeeInfo } from '../../../../../models/interfaces/employee-info';
import { DocumentFileType } from '../../../../../models/enums/uploader-item';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StoreHelper } from '../../../../../core/helpers/store.helper';
import { NotificationService } from '../../../../../services/notification.service';

@Component({
    selector: 'team-member',
    templateUrl: './team-member.component.html',
    styleUrls: ['./team-member.component.scss'],
})
export class TeamMemberComponent implements OnChanges {
    imgTypes: DocumentFileType[] = [
        DocumentFileType.JPEG, DocumentFileType.JPG, DocumentFileType.PNG,
    ];
    form: FormGroup;
    @Input() activeLanguage: string;
    @Input() data: IEmployeeInfo;
    @Output() update = new EventEmitter<IEmployeeInfo>();
    @Output() delete = new EventEmitter<string>();

    constructor(
        private fb: FormBuilder,
        private notificationService: NotificationService,
    ) { }

    ngOnChanges(): void {
        if (!this.data || !this.activeLanguage) return;
        this.initForm(this.data);
    }

    initForm(employee?: IEmployeeInfo): void {
        const value: IEmployeeInfoValue = employee.values.find(e => e.lang === this.activeLanguage);
        if (!value) {
            this.form = this.fb.group({
                firstName: ['', Validators.required],
                lastName: ['', Validators.required],
                position: ['', Validators.required],
                description: ['', Validators.required],
                photo: [employee.photo || '', Validators.required],
                isHead: [employee.isHead, Validators.required],
                showOnSite: [employee.showOnSite || false, Validators.required],
            });
        } else {
            this.form = this.fb.group({
                firstName: [value.firstName, Validators.required],
                lastName: [value.lastName, Validators.required],
                position: [value.position, Validators.required],
                description: [value.description, Validators.required],
                photo: [employee.photo, Validators.required],
                isHead: [employee.isHead, Validators.required],
                showOnSite: [employee.showOnSite, Validators.required],
            });
        }
    }

    onFileChanged(data: string): void {
        this.form.controls['photo'].setValue(data);
        this.form.controls['photo'].markAsDirty();
    }

    onSubmit(): void {
        if (!this.form.get('photo').value) {
            this.notificationService.snackbar('Please add image');
            return;
        }
        if (this.form.invalid) return;
        const v: IClientEmployeeInfo = this.form.value;
        const index = this.data.values.findIndex(v => v.lang === this.activeLanguage);
        let employee = <IEmployeeInfo>{
            ...this.data,
            photo: v.photo,
            isHead: v.isHead,
            showOnSite: this.form.value.showOnSite,
        };
        if (index > -1) {
            employee = {
                ...employee,
                values: StoreHelper.updateStoreArrayObject(employee.values,
                    index,
                    <IEmployeeInfoValue>{
                        lang: this.activeLanguage,
                        description: v.description,
                        firstName: v.firstName,
                        lastName: v.lastName,
                        position: v.position,
                    },
                ),
            };
        } else {
            employee = {
                ...employee,
                values: [
                    ...employee.values,
                    <IEmployeeInfoValue>{
                        lang: this.activeLanguage,
                        description: v.description,
                        firstName: v.firstName,
                        lastName: v.lastName,
                        position: v.position,
                    },
                ],
            };
        }
        this.update.emit(employee);
    }

    cancel(): void {
        this.ngOnChanges();
    }
}
