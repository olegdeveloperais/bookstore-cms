import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store';
import { AddEmployeeAction, EditEmployeeAction, DeleteEmployeeAction } from '../../../../store/team/team.action';
import { Observable } from 'rxjs';
import { getTeamState } from '../../../../store/team/team.reducer';
import { map } from 'rxjs/operators';
import { IEmployeeInfo } from '../../../../models/interfaces/employee-info';
import { fadeIn } from '../../../../core/animations/fade.animation';
import { getTranslatorState } from '../../../../store/translator/translator.reducer';
import { EnableTranslatorSelectorAction } from '../../../../store/translator/translator.action';

@Component({
    selector: 'team',
    templateUrl: './team.component.html',
    styleUrls: ['./team.component.scss'],
    animations: [fadeIn],
})
export class TeamComponent implements OnInit {

    filter: string;
    team$: Observable<IEmployeeInfo[]>;
    isLoading$: Observable<boolean>;
    activeLang$: Observable<string>;

    constructor(private store: Store<AppState>) { }

    ngOnInit(): void {
        this.store.dispatch(new EnableTranslatorSelectorAction(true));
        this.team$ = this.store.select(getTeamState).pipe(map(s => s.team));
        this.isLoading$ = this.store.select(getTeamState).pipe(map(s => s.isTeamLoading));
        this.activeLang$ = this.store.select(getTranslatorState).pipe(map(s => s.activeLanguage));
    }

    addTeamMember(): void {
        this.store.dispatch(new AddEmployeeAction());
    }

    onSearch(value: string): void {
        this.filter = value;
    }

    onDelete(id: string): void {
        this.store.dispatch(new DeleteEmployeeAction(id));
    }

    onUpdate(employee: IEmployeeInfo): void {
        this.store.dispatch(new EditEmployeeAction(employee));
    }
}
