import { Component, OnInit } from '@angular/core';
import { fadeIn } from '../../../../core/animations/fade.animation';
import { Observable } from 'rxjs/Observable';
import { IFeedback } from '../../../../models/interfaces/feedback';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store';
import { getFeedbacksState } from '../../../../store/feedbacks/feedbacks.reducer';
import { map } from 'rxjs/operators';
import {
    DeleteFeedbackAction,
    LoadFeedbackAttachAction,
    AddEmailAction, DeleteEmailAction,
} from '../../../../store/feedbacks/feedbacks.action';
import { EnableTranslatorSelectorAction } from '../../../../store/translator/translator.action';

@Component({
    selector: 'feedback',
    templateUrl: './feedback.component.html',
    styleUrls: ['./feedback.component.scss'],
    animations: [fadeIn],
})
export class FeedbackComponent implements OnInit {
    feedbacks$: Observable<IFeedback[]>;
    isLoading$: Observable<boolean>;
    emails$: Observable<string[]>;
    displayedColumns: string[] = ['name', 'email', 'message', 'file', 'delete'];

    constructor(
        private store: Store<AppState>,
    ) { }

    ngOnInit(): void {
        this.store.dispatch(new EnableTranslatorSelectorAction(false));
        this.feedbacks$ = this.store.select(getFeedbacksState).pipe(map(s => s.feedbacks));
        this.emails$ = this.store.select(getFeedbacksState).pipe(map(s => s.emails));
        this.isLoading$ = this.store.select(getFeedbacksState).pipe(map(s => s.isFeedbacksLoading));
    }

    onDeleteFeedback(id: string, e: MouseEvent): void {
        e.preventDefault();
        e.stopPropagation();
        this.store.dispatch(new DeleteFeedbackAction(id));
    }

    onDownloadFile(feedback: IFeedback): void {
        this.store.dispatch(new LoadFeedbackAttachAction(feedback));
    }

    onAddEmail(): void {
        this.store.dispatch(new AddEmailAction());
    }

    onDeleteItem(email: string): void {
        this.store.dispatch(new DeleteEmailAction(email));
    }
}
