import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeedbackComponent } from './feedback.component';
import {
    MatTableModule, MatIconModule,
    MatCardModule, MatTooltipModule, MatProgressBarModule,
} from '@angular/material';

@NgModule({
    imports: [
        CommonModule,
        MatTableModule,
        MatIconModule,
        MatProgressBarModule,
        MatCardModule,
        MatTooltipModule,
    ],
    declarations: [
        FeedbackComponent,
    ],
    exports: [FeedbackComponent],
})
export class FeedbackModule { }
