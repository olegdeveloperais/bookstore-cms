import { Component, OnInit } from '@angular/core';
import { fadeIn } from '../../../../core/animations/fade.animation';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/index';
import { EnableTranslatorSelectorAction } from '../../../../store/translator/translator.action';

@Component({
    selector: 'welcome',
    templateUrl: 'welcome.component.html',
    styleUrls: ['welcome.component.scss'],
    animations: [fadeIn],
})
export class WelcomeComponent implements OnInit {
    constructor(
        private store: Store<AppState>,
    ) { }

    ngOnInit(): void {
        this.store.dispatch(new EnableTranslatorSelectorAction(false));
    }

}
