import { NgModule } from '@angular/core';
import { WelcomeComponent } from './welcome.component';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        WelcomeComponent,
    ],
    exports: [WelcomeComponent],
})
export class WelcomeModule { }
