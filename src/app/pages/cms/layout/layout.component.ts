import { Component, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/index';
import { SetScrollContainerAction } from '../../../store/navigation/navigation.action';

@Component({
    selector: 'layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements AfterViewInit {
    @ViewChild('container') containerRef: ElementRef;

    constructor(
        private store: Store<AppState>,
    ) { }

    ngAfterViewInit(): void {
        this.store.dispatch(new SetScrollContainerAction(this.containerRef.nativeElement));
    }
}