import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { map, skipWhile, switchMap } from 'rxjs/operators';
import { AppState } from '../../../../store/index';
import { LogoutAction } from '../../../../store/auth/auth.action';
import { getTranslatorState } from '../../../../store/translator/translator.reducer';
import { ChangeLanguageAction } from '../../../../store/translator/translator.action';
import { Router } from '@angular/router';
import { empty, combineLatest, of } from 'rxjs';

@Component({
    selector: 'top-nav',
    templateUrl: './topnav.component.html',
    styleUrls: ['./topnav.component.scss'],
})
export class TopNavComponent implements OnInit {
    languages$: Observable<string[]>;
    activeLanguage$: Observable<string>;
    isLanguagesSelectorEnabled$: Observable<boolean>;

    @Output() onToggleMenu = new EventEmitter<any>();

    constructor(
        private store: Store<AppState>,
        private router: Router,
    ) { }

    ngOnInit(): void {
        this.languages$ = this.store.select(getTranslatorState).pipe(map(s => s.languages));
        this.activeLanguage$ = this.store.select(getTranslatorState).pipe(map(s => s.activeLanguage));
        this.isLanguagesSelectorEnabled$ = combineLatest(
            this.languages$,
            this.store.select(getTranslatorState).pipe(map(s => s.isSelectorEnabled)),
        ).pipe(
            map(([languages, enabled]) => languages && languages.length && enabled),
        );
    }

    toggleMenu(): void {
        this.onToggleMenu.emit();
    }

    logout(): void {
        this.store.dispatch(new LogoutAction());
    }

    onLanguageChange(language: string): void {
        this.store.dispatch(new ChangeLanguageAction(language));
    }

    goToSettings(): void {
        this.router.navigate(['settings']);
    }
}