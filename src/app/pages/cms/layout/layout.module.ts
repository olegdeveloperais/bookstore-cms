import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideNavComponent } from './sidenav/sidenav.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TopNavComponent } from './topnav/topnav.component';
import { MatIconModule, MatButtonModule, MatSelectModule } from '@angular/material';
import { AppCoreModule } from '../../../core/core.module';
import { LayoutComponent } from './layout.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        CommonModule,
        AppCoreModule,
        MatIconModule,
        MatButtonModule,
        MatSelectModule,
    ],
    declarations: [
        LayoutComponent,
        TopNavComponent,
        SideNavComponent,
    ],
    exports: [
        LayoutComponent,
    ],
})
export class LayoutModule { }