import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { widthInOut } from '../../../../core/animations/width.animation';
import { IMenuItem } from '../../../../models/interfaces/menu';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store';
import { Observable } from 'rxjs';
import { map, skipWhile } from 'rxjs/operators';
import { getTranslatorState } from '../../../../store/translator/translator.reducer';

@Component({
    selector: 'side-nav',
    templateUrl: './sidenav.component.html',
    styleUrls: ['./sidenav.component.scss'],
    animations: [widthInOut],
})
export class SideNavComponent implements OnInit, OnDestroy {
    isVisible: boolean = true;
    activeLink: string = '';
    routerSub: Subscription;
    languages$: Observable<string[]>;

    items: IMenuItem[] = [
    ];

    constructor(
        private router: Router,
        private store: Store<AppState>,
    ) { }

    ngOnInit(): void {
        this.routerSub = this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                const link = event.url;
                this.activeLink = link;
            }
        });
        this.activeLink = this.router.url;
        this.languages$ = this.store.select(getTranslatorState).pipe(map(s => s.languages));
    }

    ngOnDestroy(): void {
        if (this.routerSub) this.routerSub.unsubscribe();
    }

    toggle(): void {
        this.isVisible = !this.isVisible;
    }
}




