import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { IDictionaryItem } from '../models/interfaces/dictionary-item';
import { environment } from '../../environments/environment';
import { IMediaItem } from '../models/interfaces/media-item';
import { IEmployeeInfo } from '../models/interfaces/employee-info';
import { IProject } from '../models/interfaces/project';
import { ISEOSettings } from '../models/interfaces/seo-settings';
import { IFeedback } from '../models/interfaces/feedback';
import { map } from 'rxjs/operators';
import { ITechnology } from '../models/interfaces/technology';
import { ITestimonial } from '../models/interfaces/testimonials';
import { IBook } from '../models/interfaces/book';

@Injectable({
    providedIn: 'root',
})
export class HttpService {

    constructor(
        private http: HttpClient,
    ) { }

    get apiUrl(): string {
        return `${environment.apiUrl}/api`;
    }

    editBook(book: IBook): Observable<void> {
        return this.http.put<void>(`${this.apiUrl}/books/update`, book);
    }

    deleteBook(id: string): Observable<void> {
        return this.http.delete<void>(`${this.apiUrl}/books/${id}`);
    }

    addBook(book: IBook): Observable<IBook> {
        return this.http.post<IBook>(`${this.apiUrl}/books/add`, book);
    }

    loadBooks(body: {
        index: number,
        size: number,
        search: string,
    }): Observable<{
        books: IBook[],
        booksCount: number,
    }> {
        return this.http.put<{ books: IBook[], booksCount: number }>(`${this.apiUrl}/books`, body);
    }

    editEmployee(employee: IEmployeeInfo): Observable<void> {
        return this.http.put<void>(`${this.apiUrl}/team`, employee);
    }

    deleteEmployee(id: string): Observable<void> {
        return this.http.delete<void>(`${this.apiUrl}/team/${id}`);
    }

    addEmployee(): Observable<IEmployeeInfo> {
        return this.http.post<IEmployeeInfo>(`${this.apiUrl}/team`, {});
    }

    loadTeam(): Observable<IEmployeeInfo[]> {
        return this.http.get<IEmployeeInfo[]>(`${this.apiUrl}/team`);
    }

    loadDictionary(lang: string): Observable<IDictionaryItem[]> {
        return this.http.get<IDictionaryItem[]>(`${this.apiUrl}/translator/dictionary?lang=${lang}`);
    }

    addDictionaryItem(item: IDictionaryItem): Observable<{ id: string }> {
        return this.http.post<{ id: string }>(`${this.apiUrl}/translator/dictionary`, item);
    }

    updateDictionaryItem(item: IDictionaryItem): Observable<IDictionaryItem> {
        return this.http.put<IDictionaryItem>(`${this.apiUrl}/translator/dictionary`, item);
    }

    deleteDictionaryItem(id: string): Observable<void> {
        return this.http.delete<void>(`${this.apiUrl}/translator/dictionary/${id}`);
    }

    loadMedia(): Observable<IMediaItem[]> {
        return this.http.get<IMediaItem[]>(`${this.apiUrl}/media`);
    }

    addMediaItem(): Observable<IMediaItem> {
        return this.http.post<IMediaItem>(`${this.apiUrl}/media`, {});
    }

    updateMediaItem(item: IMediaItem): Observable<void> {
        return this.http.put<void>(`${this.apiUrl}/media`, item);
    }

    deleteMediaItem(id: string): Observable<void> {
        return this.http.delete<void>(`${this.apiUrl}/media/${id}`);
    }

    loadSEOSettings(): Observable<ISEOSettings[]> {
        return this.http.get<ISEOSettings[]>(`${this.apiUrl}/seo`);
    }

    addSEOSettings(): Observable<ISEOSettings> {
        return this.http.post<ISEOSettings>(`${this.apiUrl}/seo`, {});
    }

    updateSEOSettings(settings: ISEOSettings): Observable<void> {
        return this.http.put<void>(`${this.apiUrl}/seo`, settings);
    }

    deleteSEOSettings(id: string): Observable<void> {
        return this.http.delete<void>(`${this.apiUrl}/seo/${id}`);
    }

    loadLanguages(): Observable<string[]> {
        return this.http.get<string[]>(`${this.apiUrl}/translator/languages`);
    }

    addLanguage(lang: string): Observable<void> {
        return this.http.post<void>(`${this.apiUrl}/translator/languages/${lang}`, {});
    }

    deleteLanguage(lang: string): Observable<void> {
        return this.http.delete<void>(`${this.apiUrl}/translator/languages/${lang}`);
    }

    loadFeedbacks(): Observable<IFeedback[]> {
        return this.http.get<IFeedback[]>(`${this.apiUrl}/feedbacks`);
    }

    deleteFeedback(id: string): Observable<void> {
        return this.http.delete<void>(`${this.apiUrl}/feedbacks/${id}`);
    }

    loadEmails(): Observable<string[]> {
        return this.http.get<string[]>(`${this.apiUrl}/feedbacks/emails`);
    }

    addEmail(email: string): Observable<void> {
        return this.http.post<void>(`${this.apiUrl}/feedbacks/emails/${email}`, {});
    }

    deleteEmail(email: string): Observable<void> {
        return this.http.delete<void>(`${this.apiUrl}/feedbacks/emails/${email}`);
    }

    loadTechnologies(): Observable<ITechnology[]> {
        return this.http.get<ITechnology[]>(`${this.apiUrl}/technologies`);
    }

    addTechnology(): Observable<ITechnology> {
        return this.http.post<ITechnology>(`${this.apiUrl}/technologies`, {});
    }

    updateTechnology(settings: ITechnology): Observable<void> {
        return this.http.put<void>(`${this.apiUrl}/technologies`, settings);
    }

    deleteTechnology(id: string): Observable<void> {
        return this.http.delete<void>(`${this.apiUrl}/technologies/${id}`);
    }

    loadTestimonials(): Observable<ITestimonial[]> {
        return this.http.get<ITestimonial[]>(`${this.apiUrl}/testimonials`);
    }

    addTestimonial(): Observable<ITestimonial> {
        return this.http.post<ITestimonial>(`${this.apiUrl}/testimonials`, {});
    }

    updateTestimonial(testimonials: ITestimonial): Observable<void> {
        return this.http.put<void>(`${this.apiUrl}/testimonials`, testimonials);
    }

    deleteTestimonial(id: string): Observable<void> {
        return this.http.delete<void>(`${this.apiUrl}/testimonials/${id}`);
    }
}