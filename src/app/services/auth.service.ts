import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from './local-storage.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ICredentials } from '../models/interfaces/credentials';
import { StorageItem } from '../models/enums/storage-item';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    constructor(
        private http: HttpClient,
        private localStorage: LocalStorageService,
        private router: Router,
    ) { }

    get apiUrl(): string {
        return `${environment.apiUrl}/api`;
    }

    login(creds: ICredentials): Observable<any> {
        // tslint:disable-next-line:max-line-length
        // return this.http.get(`${this.apiUrl}/access-manager/login?username=${creds.username}&password=${creds.password}`, { responseType: 'text' });
        return this.http.post(`${this.apiUrl}/user/login/`, creds, { responseType: 'text' });
    }

    logout(): void {
        this.localStorage.removeItem(StorageItem.TOKEN);
        this.router.navigate(['/login']);
    }
}