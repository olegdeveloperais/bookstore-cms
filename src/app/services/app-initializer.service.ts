import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../store';
import { LoadTeamAction } from '../store/team/team.action';
import { LoadBooksAction } from '../store/books/books.action';
import { LoadFeedbacksAction, LoadEmailsAction } from '../store/feedbacks/feedbacks.action';
import { LoadLanguagesAction } from '../store/translator/translator.action';
import { LoadSEOSettingsAction } from '../store/seo/seo.action';
import { LoadMediaAction } from '../store/media/media.action';
import { LoadTechnologiesAction } from '../store/technologies/technologies.action';

@Injectable({
    providedIn: 'root',
})
export class AppInitializerService {

    constructor(
        private store: Store<AppState>,
    ) { }

    init(): void {
        this.store.dispatch(new LoadLanguagesAction());
        this.store.dispatch(new LoadSEOSettingsAction());
        this.store.dispatch(new LoadMediaAction());
        this.store.dispatch(new LoadFeedbacksAction());
        this.store.dispatch(new LoadEmailsAction());
        this.store.dispatch(new LoadTeamAction());
        this.store.dispatch(new LoadBooksAction());
        this.store.dispatch(new LoadTechnologiesAction());
    }

}