import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injector } from '@angular/core';
import { throwError } from 'rxjs';
import 'rxjs/add/operator/catch';
import { LocalStorageService } from '../local-storage.service';
import { StorageItem } from '../../models/enums/storage-item';
import { AuthService } from '../auth.service';

@Injectable({
    providedIn: 'root',
})
export class AccessInterceptor implements HttpInterceptor {
    constructor(
        private injector: Injector,
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const ls = this.injector.get(LocalStorageService);
        const token = ls.getItem(StorageItem.TOKEN);
        if (token) {
            req = req.clone({
                setHeaders: {
                    Authorization: `Bearer ${token}`,
                },
            });
        }
        return next.handle(req).catch(err => {
            const status = err.status;

            if (status === 401) {
                this.removeUserCredentials();
                return throwError(err);
            }

            return throwError(err);
        });

    }

    removeUserCredentials(): void {
        const authService = this.injector.get(AuthService);
        authService.logout();
    }

}