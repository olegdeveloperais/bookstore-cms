export const books = [{
        _id: `1`,
        img: 'https://images.booksense.com/images/319/673/9781451673319.jpg',
        author: 'Ray Bradbury',
        title: 'Fahrenheit 451',
        description: `Fahrenheit 451 is a dystopian novel by American writer Ray Bradbury, ` +
            `first published in 1953. It is regarded as one of his best works. The novel presents ` +
            `a future American society where books are outlawed and "firemen" burn any that are ` +
            `found. The book's tagline explains the title: "Fahrenheit 451 – the temperature at ` +
            `which book paper catches fire, and burns..."`,
        price: 16.99,
    },
    {
        _id: `2`,
        img: 'https://images.booksense.com/images/979/426/9781411426979.jpg',
        author: 'Nathaniel Hawthorne',
        title: 'The Scarlet Letter',
        description: `The Scarlet Letter is an 1850 romantic work of fiction in a historical ` +
            `setting, written by Nathaniel Hawthorne, and is considered to be his magnum opus. Set in` +
            ` 17th-century Puritan Boston, during the years 1642 to 1649, it tells the story ` +
            `of Hester Prynne, who conceives a daughter through an affair and struggles ` +
            `to create a new life of repentance and dignity.`,
        price: 19.98,
    },
    {
        _id: `3`,
        img: 'https://images.booksense.com/images/615/280/9780486280615.jpg',
        author: 'Mark Twain',
        title: 'Adventures of Huckleberry Finn',
        description: `Adventures of Huckleberry Finn (or, in more recent editions, The Adventures ` +
            `of Huckleberry Finn) is a novel by Mark Twain, first published in the United Kingdom in ` +
            `December 1884 and in the United States in February 1885. Commonly named among the Great ` +
            `American Novels, the work is among the first in major American literature to be written ` +
            `throughout in vernacular English, characterized by local color regionalism.`,
        price: 17.99,
    },
    {
        _id: `4`,
        img: 'https://images.booksense.com/images/319/673/9781451673319.jpg',
        author: 'Ray Bradbury',
        title: 'Fahrenheit 451',
        description: `Fahrenheit 451 is a dystopian novel by American writer Ray Bradbury, ` +
            `first published in 1953. It is regarded as one of his best works. The novel presents ` +
            `a future American society where books are outlawed and "firemen" burn any that are ` +
            `found. The book's tagline explains the title: "Fahrenheit 451 – the temperature at ` +
            `which book paper catches fire, and burns..."`,
        price: 16.99,
    },
    {
        _id: `5`,
        img: 'https://images.booksense.com/images/979/426/9781411426979.jpg',
        author: 'Nathaniel Hawthorne',
        title: 'The Scarlet Letter',
        description: `The Scarlet Letter is an 1850 romantic work of fiction in a historical ` +
            `setting, written by Nathaniel Hawthorne, and is considered to be his magnum opus. Set in` +
            ` 17th-century Puritan Boston, during the years 1642 to 1649, it tells the story ` +
            `of Hester Prynne, who conceives a daughter through an affair and struggles ` +
            `to create a new life of repentance and dignity.`,
        price: 19.98,
    },
    {
        _id: `6`,
        img: 'https://images.booksense.com/images/615/280/9780486280615.jpg',
        author: 'Mark Twain',
        title: 'Adventures of Huckleberry Finn',
        description: `Adventures of Huckleberry Finn (or, in more recent editions, The Adventures ` +
            `of Huckleberry Finn) is a novel by Mark Twain, first published in the United Kingdom in ` +
            `December 1884 and in the United States in February 1885. Commonly named among the Great ` +
            `American Novels, the work is among the first in major American literature to be written ` +
            `throughout in vernacular English, characterized by local color regionalism.`,
        price: 17.99,
    },
    {
        _id: `7`,
        img: 'https://images.booksense.com/images/319/673/9781451673319.jpg',
        author: 'Ray Bradbury',
        title: 'Fahrenheit 451',
        description: `Fahrenheit 451 is a dystopian novel by American writer Ray Bradbury, ` +
            `first published in 1953. It is regarded as one of his best works. The novel presents ` +
            `a future American society where books are outlawed and "firemen" burn any that are ` +
            `found. The book's tagline explains the title: "Fahrenheit 451 – the temperature at ` +
            `which book paper catches fire, and burns..."`,
        price: 16.99,
    },
    {
        _id: `8`,
        img: 'https://images.booksense.com/images/979/426/9781411426979.jpg',
        author: 'Nathaniel Hawthorne',
        title: 'The Scarlet Letter',
        description: `The Scarlet Letter is an 1850 romantic work of fiction in a historical ` +
            `setting, written by Nathaniel Hawthorne, and is considered to be his magnum opus. Set in` +
            ` 17th-century Puritan Boston, during the years 1642 to 1649, it tells the story ` +
            `of Hester Prynne, who conceives a daughter through an affair and struggles ` +
            `to create a new life of repentance and dignity.`,
        price: 19.98,
    },
    {
        _id: `9`,
        img: 'https://images.booksense.com/images/615/280/9780486280615.jpg',
        author: 'Mark Twain',
        title: 'Adventures of Huckleberry Finn',
        description: `Adventures of Huckleberry Finn (or, in more recent editions, The Adventures ` +
            `of Huckleberry Finn) is a novel by Mark Twain, first published in the United Kingdom in ` +
            `December 1884 and in the United States in February 1885. Commonly named among the Great ` +
            `American Novels, the work is among the first in major American literature to be written ` +
            `throughout in vernacular English, characterized by local color regionalism.`,
        price: 17.99,
    },
    {
        _id: `10`,
        img: 'https://images.booksense.com/images/319/673/9781451673319.jpg',
        author: 'Ray Bradbury',
        title: 'Fahrenheit 451',
        description: `Fahrenheit 451 is a dystopian novel by American writer Ray Bradbury, ` +
            `first published in 1953. It is regarded as one of his best works. The novel presents ` +
            `a future American society where books are outlawed and "firemen" burn any that are ` +
            `found. The book's tagline explains the title: "Fahrenheit 451 – the temperature at ` +
            `which book paper catches fire, and burns..."`,
        price: 16.99,
    },
    {
        _id: `11`,
        img: 'https://images.booksense.com/images/979/426/9781411426979.jpg',
        author: 'Nathaniel Hawthorne',
        title: 'The Scarlet Letter',
        description: `The Scarlet Letter is an 1850 romantic work of fiction in a historical ` +
            `setting, written by Nathaniel Hawthorne, and is considered to be his magnum opus. Set in` +
            ` 17th-century Puritan Boston, during the years 1642 to 1649, it tells the story ` +
            `of Hester Prynne, who conceives a daughter through an affair and struggles ` +
            `to create a new life of repentance and dignity.`,
        price: 19.98,
    },
    {
        _id: `12`,
        img: 'https://images.booksense.com/images/615/280/9780486280615.jpg',
        author: 'Mark Twain',
        title: 'Adventures of Huckleberry Finn',
        description: `Adventures of Huckleberry Finn (or, in more recent editions, The Adventures ` +
            `of Huckleberry Finn) is a novel by Mark Twain, first published in the United Kingdom in ` +
            `December 1884 and in the United States in February 1885. Commonly named among the Great ` +
            `American Novels, the work is among the first in major American literature to be written ` +
            `throughout in vernacular English, characterized by local color regionalism.`,
        price: 17.99,
    },
];