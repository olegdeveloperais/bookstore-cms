import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';

import { Observable, of } from 'rxjs';
// import 'rxjs/add/observable/of';
import { books } from './books.const';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
    booksBase = books;
    intercept(req: HttpRequest<any>, next: HttpHandler):
        Observable<HttpEvent<any>> {
        // if (req.url == 'http://localhost:3000/api/books' && req.method == 'PUT') {
            // console.log('HTTPINTERCEPTORSERVICE - req:', req);
        //     return of(new HttpResponse({
        //         status: 200, body: {
        //             booksCount: this.booksBase.length,
        //             books: this.booksBase.slice(req.body.index * req.body.size,
        //             req.body.size * req.body.index + req.body.size),
        //         },
        //     }));
        // }


        // if (req.url.includes( '/access-manager/login?username=') && req.method == 'GET') {
        //     console.log('AUTH / HTTPINTERCEPTORSERVICE - req:', req);
        //     return of(new HttpResponse({
        //         status: 200,
        //         body: '~MOCK~TOKEN~', // TOKEN
        //     }));
        // }


        // if (req.url == 'http://localhost:3000/api/books/add' && req.method == 'PUT') {
        //     console.log('HTTPINTERCEPTORSERVICE - req:', req);
        //     return of(new HttpResponse({
        //         status: 200,
        //     }));
        // }
        // if (req.url.includes('http://localhost:3000/api/books') && req.method == 'DELETE') {
        //     console.log('HTTPINTERCEPTORSERVICE DELETE - req:', req);
        //     return of(new HttpResponse({
        //         status: 200,
        //     }));
        // }
        // if (req.url.includes('http://localhost:3000/api/books') && req.method == 'POST') {
        //     console.log('HTTPINTERCEPTORSERVICE POST - req:', req);
        //     return of(new HttpResponse({
        //         status: 200, body: {
        //             _id: `4`,
        //         },
        //     }));
        // }
        return next.handle(req);
    }
}