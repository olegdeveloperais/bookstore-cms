import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class UploaderService {

    constructor(
        private http: HttpClient,
    ) { }

    get apiUrl(): string {
        return `${environment.apiUrl}/api`;
    }

    uploadPublic(file: File): Observable<string> {
        const formData = new FormData();
        formData.append('image', file);
        return this.http.post(`${this.apiUrl}/uploader/public`, formData, { responseType: 'text' });
    }

    uploadPrivate(file: File): Observable<string> {
        const formData = new FormData();
        formData.append('image', file);
        return this.http.post(`${this.apiUrl}/uploader/private`, formData, { responseType: 'text' });
    }

    downloadFile(file: string): Observable<any> {
        return this.http.get(`${this.apiUrl}/uploader/private?file=${file}`,
            { responseType: 'blob', observe: 'response' },
        );
    }
}
