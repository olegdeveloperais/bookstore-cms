import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { JwtHelperService } from '@auth0/angular-jwt';
import { StorageItem } from '../models/enums/storage-item';

@Injectable({
    providedIn: 'root',
})
export class LocalStorageService {
    private jwtHelper: JwtHelperService = new JwtHelperService();

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
    ) { }

    supports_html5_storage(): boolean {
        if (isPlatformBrowser(this.platformId)) {
            try {
                return window && 'localStorage' in window && window['localStorage'] !== undefined;
            } catch (e) {
                console.warn('Current browser doesn\'t support local storage');
                return false;
            }
        }
    }

    setItem(key: string, value: string): boolean {
        if (isPlatformBrowser(this.platformId)) {
            if (!this.supports_html5_storage()) return false;
            localStorage.setItem(key, value);
            return true;
        }
    }

    getItem(key: string): string | undefined {
        if (isPlatformBrowser(this.platformId)) {
            if (!this.supports_html5_storage()) return;
            return localStorage.getItem(key);
        }
    }

    removeItem(key: string): boolean {
        if (isPlatformBrowser(this.platformId)) {
            if (!this.supports_html5_storage()) return false;
            localStorage.removeItem(key);
            return true;
        }
    }
}