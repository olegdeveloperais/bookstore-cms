import { Injectable } from '@angular/core';
import { NotificationService } from './notification.service';

@Injectable({
    providedIn: 'root',
})
export class ErrorService {

    constructor(
        private notify: NotificationService,
    ) { }

    resolve(msg: any): void {
        if (!msg) return;
        console.error(msg);
        switch (msg.status) {
            case 409: this.notify.snackbar('Already exists'); return;
        }
    }

}